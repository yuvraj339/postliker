<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(array('domain' => 'reseller.getpopularity.com'), function()
{
	Route::get('/', function () {
        return view('auth.login');
    });
    Route::get('home', function () {
        return redirect('auth.login');
    });
});
Route::get('/', function () {
//dd(App::environment());
    return view('home');
});
Route::get('home', function () {
    return redirect('/');
});

Auth::routes();

//Route::get('/', 'HomeController@index');
Route::group(['middleware' => 'web'], function () {

    Route::get('/dashboard', 'DashboardController@index');

    Route::get('/category/load/{maincategoryid}', 'SuperAdminController@loadSubCategories');
    Route::get('/order/{id}/status/{orderid}', 'SuperAdminController@getOrderStatus');
    Route::get('/get/price/{subCatId}/{getAmount}/{userid?}', 'AccountController@getActualAmount');

    //profile setting start
    Route::get('/dashboard/user/profile', 'DashboardController@userProfile');
    Route::get('/dashboard/user/profile/edit', 'DashboardController@editUserProfile');
    Route::post('/dashboard/user/profile/update/{id}', 'DashboardController@updateUserProfile');
    Route::get('/dashboard/change/password', 'DashboardController@changePassword');
    Route::post('/dashboard/change/password/{id}', 'DashboardController@updatePassword');

    //Overview setting start
    Route::get('/dashboard/contact-us', 'DashboardController@contactUs');
    Route::post('/dashboard/contact-us', 'DashboardController@storeContactUs');

    //Overview setting start
    Route::get('/dashboard/overview', 'DashboardController@overview');
    Route::get('/dashboard/price/info', 'DashboardController@priceInfo');
    Route::get('dashboard/notification', 'DashboardController@userNotification');

    //account setting start
    Route::get('/dashboard/account/setting', 'AccountController@index');
    Route::get('/dashboard/order/add', 'AccountController@orderAdd');
    Route::post('/dashboard/order/add', 'AccountController@orderStore');
    Route::get('/dashboard/order/history', 'AccountController@orderHistory');
//    Route::get('/dashboard/account/setting/create', 'DashboardController@changePassword');
//    Route::post('/dashboard/account/setting/store', 'DashboardController@updatePassword'
//    Route::get('/dashboard/account/setting/edit', 'DashboardController@changePassword');
//    Route::patch('/dashboard/account/setting/update', 'DashboardController@updatePassword');
//    Route::delete('/dashboard/account/setting/delete', 'DashboardController@updatePassword');

    //super admin
    Route::group(['prefix' => 'super/admin','middleware' => 'App\Http\Middleware\AdminMiddleware'], function () {
        Route::get('/', 'SuperAdminController@index');

        //user module routes
        Route::get('/user/add', 'SuperAdminController@addUser');
        Route::post('/user/add', 'SuperAdminController@storeUser');
        Route::get('/user/view', 'SuperAdminController@viewUser');

        Route::get('/user/edit/{userid}', 'SuperAdminController@editUser');
        Route::post('/user/edit/{userid}', 'SuperAdminController@updateUser');
        Route::get('/user/delete/{userid}', 'SuperAdminController@deleteUser');
        Route::get('/user/group', 'SuperAdminController@groupUser');

        //fund module
        Route::get('/fund/add', 'SuperAdminController@addFund');
        Route::post('/fund/add', 'SuperAdminController@storeFund');
        Route::get('/fund/view', 'SuperAdminController@viewFund');
        Route::post('/fund/delete', 'SuperAdminController@updateFund');

        //order history module
        Route::get('/order/add', 'SuperAdminController@addOrder');
        Route::post('/order/add', 'SuperAdminController@storeOrder');

        Route::get('/order/view', 'SuperAdminController@historyOrder');
        Route::get('/order/edit/npl/{orderid}', 'SuperAdminController@editNPLOrder');
        Route::post('/order/edit/npl/{orderid}', 'SuperAdminController@updateNPLOrder');
        Route::get('/order/view/npl', 'SuperAdminController@NPLhistoryOrder');

        Route::get('/order/edit/{orderid}', 'SuperAdminController@viewOrder');
        Route::post('/order/edit/{orderid}', 'SuperAdminController@updateOrder');

        Route::get('/order/detete/{orderid}', 'SuperAdminController@deleteOrder');

        //service module
        Route::get('/service/add', 'SuperAdminController@addService');
        Route::post('/service/add', 'SuperAdminController@storeService');

        Route::get('/service/view', 'SuperAdminController@viewService');

        Route::get('/service/edit/{serviceid}', 'SuperAdminController@editService');
        Route::post('/service/edit/{serviceid}', 'SuperAdminController@updateService');

        Route::get('/service/detete/{serviceid}', 'SuperAdminController@deleteService');

        //notification module
        Route::get('/notification/add', 'SuperAdminController@addNotification');
        Route::post('/notification/add', 'SuperAdminController@storeNotification');

        Route::get('/notification/view', 'SuperAdminController@viewNotification');

        Route::get('/notification/edit/{notificationid}', 'SuperAdminController@editNotification');
        Route::post('/notification/edit/{notificationid}', 'SuperAdminController@updateNotification');

        Route::get('/notification/detete/{notificationid}', 'SuperAdminController@deleteNotification');

        //price rule module
        Route::get('/pricerule/add', 'SuperAdminController@addPriceRule');
        Route::post('/pricerule/add', 'SuperAdminController@storePriceRule');

        Route::get('/pricerule/view', 'SuperAdminController@viewPriceRule');

        Route::get('/pricerule/edit/{priceruleid}', 'SuperAdminController@editPriceRule');
        Route::post('/pricerule/edit/{priceruleid}', 'SuperAdminController@updatePriceRule');

        Route::get('/pricerule/detete/{priceruleid}', 'SuperAdminController@deletePriceRule');

        Route::any('/service/category/add', 'SuperAdminController@categoryAdd');
        Route::get('/service/category/edit/{id}/{type}', 'SuperAdminController@categoryEdit');

        Route::any('/service/subcategory/add', 'SuperAdminController@subCategoryAdd');
        Route::get('/service/subcategory/edit/{id}/{type}', 'SuperAdminController@subCategoryEdit');
//        Route::get('/fund/view', 'SuperAdminController@viewFund');
//        Route::post('/fund/delete', 'SuperAdminController@updateFund');

        Route::get('/inbox', 'SuperAdminController@inbox');

    });
});
