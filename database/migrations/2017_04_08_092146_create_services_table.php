<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
                $table->integer('service_id')->default(0);
                $table->string('user_type');
                $table->integer('main_category_id')->default(0);
                $table->integer('sub_category_id')->default(0);
                $table->string('brand')->default(0);
                $table->string('service_name')->default(0);
                $table->string('price_per_fixed_amount');
                $table->string('min_order');
                $table->string('max_order');
                $table->string('details');
                $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
