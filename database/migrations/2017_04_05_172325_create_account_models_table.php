<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('fund_added_by');
            $table->string('fund_credit_debit_value');
            $table->integer('fund_credit_status')->default(0); //credit : add money
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_models');
    }
}
