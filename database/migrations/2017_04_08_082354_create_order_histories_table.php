<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id')->default(0);//pending // completed / processing //
            $table->string('status')->default('pending');//pending // completed / processing //
            $table->string('order_price')->nullable();
            $table->string('order_date')->nullable();
            $table->string('start_count')->nullable();
            $table->string('sub_category_id')->nullable();
            $table->string('amount')->nullable();
            $table->text('order_on_url')->nullable();
            $table->string('scrf')->default(0);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_histories');
    }
}
