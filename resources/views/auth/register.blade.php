@extends('layouts.authlayout')

@section('content')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="active">Register</li>
    </ol>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="">
                    {{--<div class="panel-heading">Register</div>--}}
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control input-sm" name="name"
                                           value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control input-sm" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('skype_id') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Contact Type</label>

                                <div class="col-md-6">
                                    {{ Form::select('skype_id', [
                                                                'skype' => 'Skype Id',
                                                                'whatsapp' => 'Whatsapp No',
                                                                'phone' => 'Phone No'
                                                                ],null,['class'=>'form-control']
                                                             ) }}
                                    @if ($errors->has('skype_id'))
                                        <span class="help-block">
                                        <strong>Please select a contact type.</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('alternative_contact_no') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Contact Type Info</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control input-sm"
                                           name="alternative_contact_no" value="{{ old('alternative_contact_no') }}"
                                           required>

                                    @if ($errors->has('alternative_contact_no'))
                                        <span class="help-block">
                                        <strong>Please enter contact type info.</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control input-sm" name="password"
                                           required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control input-sm"
                                           name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                    <a class="btn btn-primary float-right" href="{{ url('/login') }}">
                                        Login
                                    </a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
