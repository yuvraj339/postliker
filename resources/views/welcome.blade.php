@extends('layouts.layout')
@section('content')

    <!-- About -->
    <section id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">

                    <p class="lead">
                    <h2>GET POPULARITY</h2>
                    We are pleased to introduce ourselves as a social media marketing and promotions company. We cover
                    all the possible ways of effective online marketing and promotions. We target on creating awareness
                    and buzz around your brand, service or product.

                    In the recent years, social media has taken the world with a boom. It’s the most effective way to
                    show you or your business in the best light. It’s all about how you portray yourself or your
                    products in the most efficient way to gather attention and audience.

                    Using our experience and lessons learned, we can help boost you/your brand on the social media
                    platform. We work on putting extra effort into your profile management to WOW your online presence.

                    <a target="_blank" href=""> Stock Photo</a>.
                    </p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Services -->
    <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
    <section id="services" class="services bg-primary">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1">
                    <h2>Our Services</h2>
                    {{--<hr class="small">--}}
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-facebook fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Facebook</strong>
                                </h4>
                                <span>
                                    - Fan Page Likes/Fans<br>
                                    - Worldwide facebook fans (likes)<br>
                                    - Indian Facebook Fans(Targeted Indian audience)<br>
                                    - United states,turkey,russian,asian,australian &amp; more facebook fans<br>
                                    - Facebook Followers (Worldwide/Indian)<br>
                                    - Facebook Page Verification (Verified facebook page)<br>
                                    - Facebook Page Management<br>
                                    - Movie Promotion
                                </span>
                                {{--<a href="#" class="btn btn-light">Learn More</a>--}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-youtube fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Youtube</strong>
                                </h4>
                                <span>
                                    -Views<br>
                                    -Worldwide<br>
                                    -Country Targeted (usa,india,russia or more)<br>
                                    -Likes/comment/subscribers<br>
                                </span>
                                {{--<a href="#" class="btn btn-light">Learn More</a>--}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-twitter fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Twitter</strong>
                                </h4>
                                <span>
                                    -Twitter followers<br>
                                    -Worldwide <br>
                                    -Country targeted (indian,usa,arab,Russia)<br>
                                    -Twitter retweets/Favorites/trend<br>
                                </span>
                                {{--<a href="#" class="btn btn-light">Learn More</a>--}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-instagram fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Instagram</strong>
                                </h4>
                                <span>
                                    -Followers (worldwide/country targeted)<br>
                                    -Likes
                                </span>
                                {{--<a href="#" class="btn btn-light">Learn More</a>--}}
                            </div>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Callout -->

    {{--<aside class="callout">
        <div class="text-vertical-center">
            <h1>Vertically Centered Text</h1>
        </div>
    </aside>--}}

    <!-- Portfolio -->
    {{--<section id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h2>Our Work</h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img class="img-portfolio img-responsive" src="img/portfolio-1.jpg">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img class="img-portfolio img-responsive" src="img/portfolio-2.jpg">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img class="img-portfolio img-responsive" src="img/portfolio-3.jpg">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <a href="#">
                                    <img class="img-portfolio img-responsive" src="img/portfolio-4.jpg">
                                </a>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="btn btn-dark">View More Items</a>
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
--}}
    <!-- Call to Action -->
    {{--@if (Auth::check())
        <aside class="call-to-action bg-primary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h3>Login / Register</h3>

                        <a class="btn btn-lg btn-light" href="{{ url('/login') }}">Login</a>
                        <a class="btn btn-lg btn-light" href="{{ url('/register') }}">Register</a>

                    </div>
                </div>
            </div>
        </aside>
    @endif--}}
    <!-- Map -->
    <section id="contact" class="map">
        <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
        <br/>
        <small>
            <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a>
        </small>
    </section>

@endsection