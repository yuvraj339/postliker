<script>

    $(document).ready(function () {
        $('#mainCategory').click(function () {
            loadState($(this).find(':selected').val())
        });
    });

    function loadState(mainCategoryId) {
        $("#subCategory").children().remove();
        var base_url = window.location.origin;
        $.ajax({
            type: "get",
            url: base_url + "/category/load/" + mainCategoryId
//                data: "get=state&countryId=" + countryId
        }).done(function (result) {
            $(result).each(function () {
                $("#subCategory").append($('<option>', {
                    value: this.service_id,
                    text: this.sub_service_name
                }));
            })
        });
    }

    $(document).ready(function () {
        $('#subCategory').click(function () {

            sessionStorage.setItem("subcategoryValue",$(this).find(':selected').val());
            sessionStorage.setItem("subcategoryText",$(this).find(':selected').text());
//            alert(sessionStorage.getItem("subcategoryText"));
//            alert(sessionStorage.getItem("subcategoryValue"));
        });
//        $('#subCategory').find(':selected').val(sessionStorage.getItem("subcategoryValue"));
//        $('#subCategory').find(':selected').text(sessionStorage.getItem("subcategoryText"))
    });

    function calculateTotalAmount()
    {
        var userId = '';
        if('{{Auth::user()->user_type == 'superadmin'}}')
        {
            userId = $('#userlist').find(':selected').val();;
        }
        var subCategory = $('#subCategory').find(':selected').val();
        if( subCategory === '')
        {
            document.getElementById("putTotalAmount").innerHTML  = 'Please select sub category';
        }
        else
        {
            document.getElementById("putTotalAmount").innerHTML  = '';
        }
        var base_url = window.location.origin;
        $.ajax({
            type: "get",
            url: base_url + "/get/price/"+subCategory+'/'+$('#getAmount').val()+'/'+userId,
            dataType: 'json',
            success: function (jsonData) {
//                alert(jsonData.totalAmount);
                document.getElementById("putTotalAmount").innerHTML = '<b>Total paybal amount is : ' + jsonData.totalAmount + '</b>';
            }
        });
    }
</script>