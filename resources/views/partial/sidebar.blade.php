<!-- SIDEBAR USERPIC -->
<div class="profile-userpic">
    <a href="{{url('/dashboard')}}">
        {{ Form::image(Auth::user()->profile_img != null ? url('/uploads/profileImg/'.Auth::user()->profile_img): 'img/user-dummy-pic.png', 'profile_img', ['alt'=>'Profile picture','class' => 'img-circle img-responsive']) }}
    </a>
</div>
<!-- END SIDEBAR USERPIC -->
<!-- SIDEBAR USER TITLE -->
<div class="profile-usertitle">
    <div class="profile-usertitle-name">
        @if (Auth::check())
            {{ucfirst(Auth::user()->name)}}
        @endif
    </div>
   {{-- <div class="profile-usertitle-job">
        Developer
    </div>--}}
</div>
<!-- END SIDEBAR USER TITLE -->
<!-- SIDEBAR BUTTONS -->
<div class="profile-userbuttons">
    <a href="{{url('/dashboard/user/profile')}}" class="btn btn-default btn-sm">
        <i class="glyphicon glyphicon-User"></i>
        View Profile
    </a>
    {{--<button type="button" class="btn btn-danger btn-sm">Message</button>--}}
</div>
<!-- END SIDEBAR BUTTONS -->
<!-- SIDEBAR MENU -->
<div class="profile-usermenu">
    <ul class="nav">
        <li class="{{Session()->get('selectedPage') == 'Overview' ? 'active' : ''}}">
            <a href="{{url('/dashboard/overview')}}">
                <i class="glyphicon glyphicon-home"></i>
                Overview </a>
        </li>
        <li class="{{Session()->get('selectedPage') == 'Account' ? 'active' : ''}}">
            <a href="{{url('/dashboard/account/setting')}}">
                <i class="glyphicon glyphicon-user"></i>
                Account History </a>
        </li>
        <li class="{{Session()->get('selectedPage') == 'Order Add' ? 'active' : ''}}">
            <a href="{{url('/dashboard/order/add')}}">
                <i class="glyphicon glyphicon-plus"></i>
                Add Order </a>
        </li>
        <li class="{{Session()->get('selectedPage') == 'Order History' ? 'active' : ''}}">
            <a href="{{url('/dashboard/order/history')}}">
                <i class="glyphicon glyphicon-th-list"></i>
                Order History </a>
        </li>
        <li class="{{Session()->get('selectedPage') == 'Price Info' ? 'active' : ''}}">
            <a href="{{url('/dashboard/price/info')}}">
                <i class="glyphicon glyphicon-euro"></i>
                Pricing & Info </a>
        </li>
        <li class="{{Session()->get('selectedPage') == 'Notification' ? 'active' : ''}}">
            <a href="{{url('/dashboard/notification')}}">
                <i class="glyphicon glyphicon-envelope"></i>
                Notifications </a>
        </li>
        <li class="{{Session()->get('selectedPage') == 'Contact Us' ? 'active' : ''}}">
            <a href="{{url('/dashboard/contact-us')}}">
                <i class="glyphicon glyphicon-user"></i>
                Contact us </a>
        </li>
    </ul>
</div>
<!-- END MENU -->