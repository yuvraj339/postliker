<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Liker') }}</title>

    <!-- Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

    <style>
        body {
            background-color: lightslategrey !important;
        }
    </style>

    <link href="{{asset('css/admin-dashboard.css')}}" rel="stylesheet">
    <link href="{{url('/css/bootstrap.min.css')}}" rel="stylesheet">
{{--    <link href="{{url('/css/stylish-portfolio.css')}}" rel="stylesheet">--}}
    <link href="{{url('/css/jquery.dataTables.min.css')}}" rel="stylesheet">


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>