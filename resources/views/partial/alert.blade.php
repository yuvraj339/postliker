@if($alert = Session::get('am-alert'))
    <div class="alert alert-dismissible alert-{{ $alert['type'] }}" style="height: 50px">
        <button type="button" class="close" data-dismiss="alert" onclick="alerthide();">&times;</button>
    {{--<div class="am-alert am-alert-{{ $alert['type'] }}" data-am-alert="" >--}}
        {{--<button type="button" class="am-close" onclick="alerthide()">&times;</button>--}}
       {{-- @if($alert['type'] == 'success')
            <span>Success</span>
        @elseif($alert['type'] == 'warning')
            <span> Warning </span>
        @elseif($alert['type'] == 'danger')
            <span>Wrong</span>
        @else
            <span>Prompt</span>
        @endif--}}
        @foreach($alert['data'] as $item)
            <span> {{ $item }}</span>
        @endforeach
    </div>
@endif
@if($alert = Session::get('am-alert'))
    <script>
//        $.noConflict();

        function alerthide() {
            $('.alert').hide(100);
        }
        $(".alert-{{ $alert['type'] }}").fadeOut(5000);
    </script>
@endif