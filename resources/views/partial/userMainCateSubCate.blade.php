@if(Auth::user()->user_type == 'superadmin')
    @if($type == 'service')
        <div class="form-group">

            <label for="selector1" class="col-sm-2 control-label">User Group</label>
            <div class="col-sm-8">
                {{ Form::select('user_type', [
                            'general' => 'General',
                            'retailer' => 'Retailer'],null,['class'=>'form-control1']
                         ) }}
            </div>
        </div>
    @else
        <div class="form-group">
            <label for="userlist" class="col-sm-2 control-label">Select User<b class="text-danger">*</b></label>
            <div class="col-sm-8">
                {{ Form::select('user_id', $userList,null,['id'=>'userlist','class'=>'selectpicker form-control','data-live-search'=>'true']
                         ) }}
                @if ($errors->has('user_id'))
                    <span class="text-danger"> Please select one user </span>
                @endif
            </div>
        </div>
    @endif
    <div class="form-group">

        <label for="selector1" class="col-sm-2 control-label">Select Category<b class="text-danger">*</b></label>
        <div class="col-sm-8">
            {{ Form::select('main_category_id', $mainCategories ,null,['class'=>'form-control1','id'=>'mainCategory']
                     ) }}
            @if ($errors->has('main_category_id'))
                <span class="text-danger"> {{ $errors->first('main_category_id') }} </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <label for="selector1" class="col-sm-2 control-label">Sub Category<b class="text-danger">*</b></label>
        <div class="col-sm-8">
            {{ Form::select('sub_category_id', [''=>'Select category'] ,null,['class'=>'form-control1','id'=>'subCategory']
                     ) }}
            @if ($errors->has('sub_category_id'))
                <span class="text-danger"> Please select one sub category  </span>
            @endif
        </div>
    </div>
@else
    <div class="form-group">

        <label for="selector1" class="col-sm-2 control-label">Select Category<b class="text-danger">*</b></label>
        <div class="col-sm-8">
            {{ Form::select('main_category_id', $mainCategories ,null,['class'=>'form-control input-sm','id'=>'mainCategory']
                     ) }}
            @if ($errors->has('main_category_id'))
                <span class="text-danger"> Please select main category  </span>
            @endif
        </div>
    </div>

{{--use inject if needed--}}
    {{--@if($subCategory != '')
        <div class="form-group">

            <label for="selector1" class="col-sm-2 control-label">Sub Category</label>
            <div class="col-sm-8">
                {{ Form::input('text', 'sub_category_id',$subCategory,['class'=>'form-control1','readonly','id'=>'subCategory']) }}
            </div>
        </div>
    @else--}}
    <div class="form-group">

        <label for="selector1" class="col-sm-2 control-label">Sub Category<b class="text-danger">*</b></label>
        <div class="col-sm-8">
            {{ Form::select('sub_category_id', [''=>'Select category'] ,null,['class'=>'form-control input-sm','id'=>'subCategory']
                     ) }}
            @if ($errors->has('sub_category_id'))
                <span class="text-danger"> Please select one sub category  </span>
            @endif
        </div>
    </div>
@endif