@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'order','levelOneLink'=>'/super/admin/','levelTwo'=>'View','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')

        <div class="blank-page">
            <table id="allorderSA" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Order Id</th>
                    {{--<th>PL-Id</th>--}}
                    <th>User Name</th>
                    <th>Service Name</th>
                    <th>Amount / Price</th>
                    <th>Order on URL</th>
                    <th>Date</th>
                    <th>S/C/R/F</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($orderDetails as $order)
                    <tr>
                        <td>GP-{{$order->id}}</td>
                        {{--                        <td>{{$order->order_id}}</td>--}}
                        <td>{{$userDetails[$order->user_id]}}</td>
                        <td>{{$serviceDetails[$order->sub_category_id]}}</td>
                        <td>{{$order->amount}} / {{$order->order_price}}</td>

                        <td>{{$order->order_on_url}}</td>
                        <td>{{$order->created_at}}</td>
                        <td>
                            {{$order->start_count}} /
                            {{ $order->status == 'Completed' ? (int)$order->start_count + (int)$order->amount : 0 }} /
                            0 /
                            {{(int)$order->start_count + (int)$order->amount}}
                        </td>
                        <td>
                            {{$order->status }}
                        </td>
                        <td>
                            <a href="{{url('/super/admin/order/edit/npl/'.$order->id)}}" class="btn btn-success"><b class="fa fa-pencil"></b></a>
                            {{--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">--}}
                                {{--<b class="fa fa-pencil"></b>--}}
                            {{--</button>--}}
                            <a href="{{url('/super/admin/order/detete/'.$order->id)}}" class="btn btn-danger"><b
                                        class="fa fa-trash"></b></a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allorderSA').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>

    <script type="text/javascript">
        function recheckStatus(orderId, id) {
            var base_url = window.location.origin;
            $.ajax({
                type: "get",
                url: base_url + "/order/" + id + "/status/" + orderId,
                dataType: 'json',
                async: false, //This is deprecated in the latest version of jquery must use now callbacks
                success: function (jsonData) {
                    alert(jsonData.orderStatus);
                    document.getElementById("updateStatus" + id).innerHTML = jsonData.orderStatus;
                }
            });
        }
    </script>
@endsection