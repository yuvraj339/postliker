@extends('layouts.SA-Layout')
@section('content')
@include('superAdmin.partial.breadcrumbs',['levelOne'=>'Order','levelOneLink'=>'/super/admin/','levelTwo'=>'Add','levelTwoLink'=>null])

<div class="blank">
    @include('partial.alert')
    <div class="blank-page">
        <form class="form-horizontal" role="form" method="POST" action="">
            {{csrf_field()}}

           {{-- <div class="form-group">
                <label for="selector1" class="col-sm-2 control-label">Select User</label>
                <div class="col-sm-8">
                    {{ Form::select('user_id', $userList,null,['class'=>'form-control1']
                             ) }}
                </div>
            </div>--}}

            @include('partial.userMainCateSubCate',['type'=>'order'])

            <div class="form-group">
                <label class="col-sm-2 control-label">Enter URL* </label>
                <div class="col-sm-8">
                    {{ Form::input('text', 'order_on_url',null,['class'=>'form-control1']) }}
                    @if ($errors->has('order_on_url'))
                        <span class="text-danger"> {{ $errors->first('order_on_url') }} </span>
                    @endif
                </div>
                {{--<div class="col-sm-2">
                    <p class="help-block">Your help text!</p>
                </div>--}}
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Enter Amount*</label>
                <div class="col-sm-8">
                    {{ Form::input('text', 'amount',null,['class'=>'form-control1','autocomplete'=>'off','id'=>'getAmount','onkeyup'=>'calculateTotalAmount("")']) }}
                    @if ($errors->has('amount'))
                        <span class="text-danger"> {{ $errors->first('amount') }} </span>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<p class="help-block" id="putTotalAmount"></p>--}}
                {{--</div>--}}
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-sm-8">
                    <p class="help-block" id="putTotalAmount"></p>
                </div>
            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn-primary btn" type="submit">Submit</button>
                        <button class="btn-inverse btn" type="reset">Reset</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

@endsection
@section('javascript')
    @include('partial.subCategoriesJs')
@endsection