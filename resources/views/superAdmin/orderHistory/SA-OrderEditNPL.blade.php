@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'User','levelOneLink'=>'/super/admin/','levelTwo'=>'Add fund','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="blank-page">
            {!! Form::model($orderDetails,array('url'=>'/super/admin/order/edit/npl/'.$orderDetails->id,'class' => 'form-horizontal','accept-charset'=>'UTF-8', 'enctype'=>'multipart/form-data')) !!}
            {{csrf_field()}}
            <div class="form-group">
                <label class="col-sm-2 control-label">Enter Start Count
                    {{--<small> (INR)</small>--}}
                </label>
                <div class="col-sm-8">
                    {{ Form::input('text', 'start_count',null,['class'=>'form-control1']) }}
                </div>
            </div>
            {{--<div class="form-group">
                <label class="col-sm-4 control-label">Enter value of C
                    --}}{{--<small> (INR)</small>--}}{{--
                </label>
                <div class="col-sm-7">
                    {{ Form::input('text', '',null) }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Enter value of R
                    --}}{{--<small> (INR)</small>--}}{{--
                </label>
                <div class="col-sm-7">
                    {{ Form::input('text', 'scrf',null) }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Enter value of f
                    --}}{{--<small> (INR)</small>--}}{{--
                </label>
                <div class="col-sm-7">
                    {{ Form::input('text', 'scrf',null) }}
                </div>
            </div>--}}
            <div class="form-group">
                <label class="col-sm-2 control-label">Status
                    {{--<small> (INR)</small>--}}
                </label>
                <div class="col-sm-8">
                    {{ Form::select('status', [
                        'Pending' => 'Pending',
                        'Processing' => 'Processing',
                        'In Progress' => 'In Progress',
                        'Partially completed' => 'Partially completed',
                        'Completed' => 'Completed',
                        'Canceled' => 'Canceled',
                        ],null,['class'=>'form-control1']
                     ) }}
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button type="submit" class="btn btn-success btn-sm" data-original-title="Edit this user"
                                data-toggle="Update">Update
                        </button>
                        {!! Form::reset('Reset', ['class' => 'btn btn-danger btn-sm']) !!}
                        {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
