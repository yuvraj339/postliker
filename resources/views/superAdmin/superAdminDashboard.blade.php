@extends('layouts.SA-Layout')
@section('content')

    <!--banner-->
    <div class="banner">

        <h2>
            <a href="index.html">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>Dashboard</span>
        </h2>
    </div>
    <!--//banner-->
    <!--content-->
    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1 col-md-4 ">
                <div class="top-content">
                    <h5>Tasks</h5>
                    <label>8761</label>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="content-top-1 col-md-4 ">
                <div class="top-content">
                    <h5>Points</h5>
                    <label>6295</label>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="content-top-1 col-md-4 ">
                <div class="top-content">
                    <h5>Cards</h5>
                    <label>3401</label>
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!---->


    <div class="content-mid">
        <div class="col-md-12">
            <div class="post-bottom-1 col-md-4">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <p>15k <label>Likes</label></p>
            </div>
            <div class="post-bottom-1  col-md-4">
                <a href="#"><i class="fa fa-twitter"></i></a>
                <p>20M <label>Followers</label></p>
            </div>
            <div class="post-bottom-1  col-md-4">
                <a href="#"><i class="fa fa-instagram"></i></a>
                <p>20M <label>Likes</label></p>
            </div>
            <div class="clearfix">&nbsp;</div>

        </div>

        <div class="clearfix"></div>
    </div>
    <!----->
    {{--<div class="content-bottom">

        <div class="col-md-12 post-bottom">
            <div class="post">
                <form class="text-area">
                    <textarea required="" placeholder="Post a global notification.."></textarea>
                </form>
                <div class="post-at">

                    <form class="text-sub">
                        <input type="submit" value="post">
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
         <div class="clearfix"></div>
    </div>--}}
    <!--//content-->

@endsection