<div class="banner">
    <h2>
        <a href="{{url('super/admin/')}}">Home</a>
        <i class="fa fa-angle-right"></i>
        @if($levelOne != null)
             @if($levelOneLink != null)
                <a href="{{ url($levelOneLink) }}">{{$levelOne}}</a>
            @else
                <strong>  {{$levelOne}} </strong>
            @endif
        @endif
        @if($levelTwo != null)
            <i class="fa fa-angle-right"></i>
            @if($levelTwoLink != null)
                <a href="{{ url($levelTwoLink) }}">{{$levelTwo}}</a>
            @else
                <strong>  {{$levelTwo}} </strong>
            @endif
        @endif
    </h2>
</div>