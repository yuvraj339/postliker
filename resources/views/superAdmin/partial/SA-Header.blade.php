<title>{{ config('app.name', 'Liker') }} : Super Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="keywords" content=""/>
<script type="application/x-javascript"> addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);
    function hideURLbar() {
        window.scrollTo(0, 1);
    } </script>
<link href="{{url('/')}}/superAdmin/css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" rel='stylesheet' type='text/css'/>
<!-- Custom Theme files -->
<link href="{{url('/')}}/superAdmin/css/style.css" rel='stylesheet' type='text/css'/>
<link href="{{url('/')}}/superAdmin/css/font-awesome.css" rel="stylesheet">
<style type="text/css">
    .nav.nav-second-level.collapse.in {
        padding-left: 10px;
    }

</style>
<!-- Mainly scripts -->
<!-- Custom and plugin javascript -->
{{--<script src="{{asset('js/jquery-1.12.4.js')}}" type="text/javascript"></script>--}}

<link href="{{url('/')}}/superAdmin/css/custom.css" rel="stylesheet">
<link href="{{url('/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<script src="{{url('/')}}/superAdmin/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

<script src="{{url('/')}}/superAdmin/js/jquery.metisMenu.js"></script>
<script src="{{url('/')}}/superAdmin/js/jquery.slimscroll.min.js"></script>

<script src="{{url('/')}}/superAdmin/js/custom.js"></script>
<script src="{{url('/')}}/superAdmin/js/screenfull.js"></script>
<script>
    $(function () {
        $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

        if (!screenfull.enabled) {
            return false;
        }


        $('#toggle').click(function () {
            screenfull.toggle($('#container')[0]);
        });


    });
</script>

<!--skycons-icons-->
{{--<script src="{{url('/')}}/superAdmin/js/skycons.js"></script>--}}
<!--//skycons-icons-->