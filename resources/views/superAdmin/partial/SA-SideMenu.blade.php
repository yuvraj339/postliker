<nav class="navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <h1><a class="navbar-brand" href="{{url('/dashboard')}}">SUPER ADMIN</a></h1>
    </div>
    <div class=" border-bottom">

     {{--   <div class="full-left">
            <section class="full-top">
                <button id="toggle"><i class="fa fa-arrows-alt"></i></button>
            </section>
            <form class=" navbar-left-right">
                <input type="text" value="Search..." onfocus="this.value = '';"
                       onblur="if (this.value == '') {this.value = 'Search...';}">
                <input type="submit" value="" class="fa fa-search">
            </form>
            <div class="clearfix"></div>
        </div>--}}

        @include('superAdmin.partial.notification')

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="{{url('/dashboard')}}" class=" hvr-bounce-to-right"><i
                                    class="fa fa-dashboard nav_icon "></i><span class="nav-label"> Dashboards</span> </a>
                    </li>

                    <li>
                        <a href="{{url('/super/admin/inbox')}}" class=" hvr-bounce-to-right"><i
                                    class="fa fa-inbox nav_icon "></i><span class="nav-label"> Inbox</span> </a>
                    </li>

                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-user nav_icon"></i> <span
                                    class="nav-label">Users</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('/super/admin/user/add')}}" class=" hvr-bounce-to-right"> <i
                                            class="fa fa-user-plus nav_icon"></i>Add User</a></li>

                            <li><a href="{{url('/super/admin/user/view')}}" class=" hvr-bounce-to-right"><i
                                            class="fa fa-users nav_icon"></i>View Users</a></li>

                            <li><a href="{{url('/super/admin/user/group')}}" class=" hvr-bounce-to-right"><i
                                            class="fa fa-object-group nav_icon"></i>User Group</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-money nav_icon"></i> <span
                                    class="nav-label">Fund Details</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('/super/admin/fund/add')}}" class=" hvr-bounce-to-right"> <i
                                            class="fa fa-plus-square nav_icon"></i>Add Fund</a></li>

                            <li><a href="{{url('/super/admin/fund/view')}}" class=" hvr-bounce-to-right"><i
                                            class="fa fa-newspaper-o nav_icon"></i>View Fund</a></li>

                        </ul>
                    </li>

                    {{--<li>
                        <a href="{{url('/super/admin/orderhistory')}}" class=" hvr-bounce-to-right"><i
                                    class="fa fa-history nav_icon "></i><span class="nav-label"> Order History</span> </a>
                    </li>--}}

                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-history nav_icon"></i> <span
                                    class="nav-label">Order Details</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('/super/admin/order/add')}}" class=" hvr-bounce-to-right"> <i
                                            class="fa fa-plus-square nav_icon"></i>Add Order</a></li>

                            <li><a href="{{url('/super/admin/order/view')}}" class=" hvr-bounce-to-right"><i
                                            class="fa fa-newspaper-o nav_icon"></i>View Orders</a></li>

                            <li><a href="{{url('/super/admin/order/view/npl')}}" class=" hvr-bounce-to-right"><i
                                            class="fa fa-newspaper-o nav_icon"></i>Other Orders</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-server nav_icon"></i> <span
                                    class="nav-label">Price Rules</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('/super/admin/pricerule/add')}}" class=" hvr-bounce-to-right"> <i
                                            class="fa fa-plus-square nav_icon"></i>Add Rule</a></li>

                            <li><a href="{{url('/super/admin/pricerule/view')}}" class=" hvr-bounce-to-right"><i
                                            class="fa fa-newspaper-o nav_icon"></i>View Rules</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-envelope nav_icon"></i> <span
                                    class="nav-label">Notification</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('/super/admin/notification/add')}}" class=" hvr-bounce-to-right"> <i
                                            class="fa fa-calendar-plus-o nav_icon"></i>Add Notification</a></li>

                            <li><a href="{{url('/super/admin/notification/view')}}" class=" hvr-bounce-to-right"><i
                                            class="fa fa-bell nav_icon"></i>View Notification</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-file-text nav_icon"></i> <span
                                    class="nav-label">Services</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('/super/admin/service/add')}}" class=" hvr-bounce-to-right"> <i
                                            class="fa fa-medkit nav_icon"></i>Add Service</a></li>
                            <li><a href="{{url('/super/admin/service/view')}}" class=" hvr-bounce-to-right"><i
                                            class="fa fa-list-ul nav_icon"></i>View Service</a></li>

                            <li><a href="{{url('/super/admin/service/category/add')}}" class=" hvr-bounce-to-right"> <i
                                            class="fa fa-medkit nav_icon"></i>Add Category</a></li>
                            <li><a href="{{url('/super/admin/service/subcategory/add')}}" class=" hvr-bounce-to-right"><i
                                            class="fa fa-list-ul nav_icon"></i>Add Sub Category</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</nav>