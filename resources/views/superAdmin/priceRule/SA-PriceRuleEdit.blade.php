@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'Price Rule','levelOneLink'=>'/super/admin/pricerule/view','levelTwo'=>'Edit','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="blank-page">
            {!! Form::model($priceDetails,array('url'=>'super/admin/pricerule/edit/'.$priceDetails->id,'class' => 'form-horizontal','accept-charset'=>'UTF-8')) !!}

            {{csrf_field()}}
{{--            @include('superAdmin.priceRule.SA-PriceRuleAddEditCommon',['subCategory'=>$priceDetails->sub_category_id])--}}
            @include('superAdmin.priceRule.SA-PriceRuleAddEditCommon')

            {!! Form::close() !!}
        </div>
    </div>

@endsection
@section('javascript')
 @include('partial.subCategoriesJs')
@endsection
