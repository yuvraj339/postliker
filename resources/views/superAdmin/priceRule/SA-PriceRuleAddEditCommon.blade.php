{{--@include('partial.userMainCateSubCate',['type'=>'price','subCategory'=>$subCategory])--}}
@include('partial.userMainCateSubCate',['type'=>'price'])
<div class="form-group">
    <label class="col-sm-2 control-label">Set Price On Unit</label>
    <div class="col-sm-8">
        {{ Form::input('text', 'price_on_unit',null,['class'=>'form-control1']) }}

    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Set Price</label>
    <div class="col-sm-8">
        {{ Form::input('text', 'price',null,['class'=>'form-control1']) }}

    </div>
</div>

{{--  <div class="form-group">
      <label for="selector1" class="col-sm-2 control-label">User Status</label>
      <div class="col-sm-8">
          {{ Form::select('status', [
                      '1' => 'Enable',
                      '0' => 'Disable'],null,['class'=>'form-control1']
                   ) }}
      </div>
  </div>--}}

<div class="panel-footer">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <button class="btn-primary btn" type="submit">Submit</button>
            <button class="btn-inverse btn" type="reset">Reset</button>
        </div>
    </div>
</div>
