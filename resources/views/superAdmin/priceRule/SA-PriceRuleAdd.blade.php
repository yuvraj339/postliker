@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'Price Rule','levelOneLink'=>'super/admin/pricerule/view/','levelTwo'=>'Add','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="blank-page">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{csrf_field()}}

                @include('superAdmin.priceRule.SA-PriceRuleAddEditCommon')

            </form>
        </div>
    </div>

@endsection
@section('javascript')
    @include('partial.subCategoriesJs')
@endsection
