@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'order','levelOneLink'=>'/super/admin/','levelTwo'=>'View','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')

        <div class="blank-page">
            <table id="allorderSA" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>User</th>
                    <th>Main Category</th>
                    <th>Sub Category</th>
                    <th>Price</th>
                    <th>Price On Unit</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($priceDetails as $price)
                    <tr>
                        <td>{{$tableCounter++}}</td>
                        <td>{{$userList[$price->user_id]}}</td>
                        <td>{{isset($mainCategories[$price->main_category_id]) ?$mainCategories[$price->main_category_id] : 'NA' }}</td>
                        <td>{{isset($subCategories[$price->sub_category_id]) ? $subCategories[$price->sub_category_id] : 'NA'}}</td>
                        <td>{{$price->price}}</td>
                        <td>{{$price->price_on_unit}}</td>
                        <td>
                            <a href="{{url('/super/admin/pricerule/edit/'.$price->id)}}" class="btn btn-success">View</a>
                            <a href="{{url('/super/admin/pricerule/detete/'.$price->id)}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allorderSA').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endsection