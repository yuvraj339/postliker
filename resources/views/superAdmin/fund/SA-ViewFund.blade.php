@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'Account','levelOneLink'=>'/super/admin/','levelTwo'=>'View','levelTwoLink'=>null])

    <div class="blank">
        <div class="blank-page">
            <table id="allFundSA" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>User Name</th>
                    <th>Credited / Debited By</th>
                    <th>Amount</th>
                    <th>Credit/Debit</th>
                    <th>Date</th>
                </tr>
                </thead>

                <tbody>
                @foreach($fundDetails as $fund)
                    <tr>
                        <td>{{$tableCounter++}}</td>
                        <td>{{$userDetails[$fund->user_id]}}</td>
                        <td>{{$userDetails[$fund->fund_added_by]}}</td>
                        <td>{{$fund->fund_credit_debit_value}}</td>
                        <td>{{$fund->fund_credit_status == 1 ? 'Credit' : 'Debited'}}</td>
                        <td>{{$fund->created_at}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allFundSA').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endsection