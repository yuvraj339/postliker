@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'User','levelOneLink'=>'/super/admin/','levelTwo'=>'Add fund','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="blank-page">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="selector1" class="col-sm-2 control-label">Select User</label>
                    <div class="col-sm-8">
                        {{ Form::select('user_id', $userList,null,['class'=>'selectpicker form-control','data-live-search'=>'true']
                                 ) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Transfer Fund<small> $ </small></label>
                    <div class="col-sm-8">
                        {{ Form::input('text', 'fund',null,['class'=>'form-control1']) }}
                    </div>
                    {{--<div class="col-sm-2">
                        <p class="help-block">Your help text!</p>
                    </div>--}}
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn-primary btn" type="submit">Submit</button>
                            <button class="btn-inverse btn" type="reset">Reset</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
