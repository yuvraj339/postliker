@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'Main Category','levelOneLink'=>'/super/admin/','levelTwo'=>'Add','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="blank-page">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{csrf_field()}}

                <div class="form-group">
                    <label class="col-sm-2 control-label">Service Name</label>
                    <div class="col-sm-8    ">
                        {{ Form::input('text', 'service_name',null,['class'=>'form-control1']) }}

                    </div>

                </div>

                <div class="form-group">
                    <label for="selector1" class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-8">
                        {{ Form::select('status', [
                                    '1' => 'Enable',
                                    '0' => 'Disable'],null,['class'=>'form-control1']
                                 ) }}
                    </div>
                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn-primary btn" type="submit">Submit</button>
                            <button class="btn-inverse btn" type="reset">Reset</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div> &nbsp; </div>
        <div class="blank-page">
            <table id="allMainCategory" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>Service Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($mainCategory as $service)
                    <tr>
                        <td>{{$tableCounter++}}</td>
                        <td>{{$service->service_name}}</td>
                        <td>{{$service->status == 1 ? 'Enable' : 'Disable'}}</td>
                        <td>
                            <a href="{{url('/super/admin/service/category/edit/'.$service->id.'/status')}}"
                               class="btn btn-{{$service->status == 0 ? 'success' : 'danger'}}">{{$service->status == 0 ? 'Enable' : 'Disable'}}</a>
                            <a href="{{url('/super/admin/service/category/edit/'.$service->id.'/delete')}}"
                               class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allMainCategory').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endsection
