@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'order','levelOneLink'=>'/super/admin/','levelTwo'=>'View','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')

        <div class="blank-page">
            <table id="allorderSA" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>User Type</th>
                    <th>Brand</th>
                    <th>Services</th>
                    <th>Price per 1000</th>
                    <th>Min / Max Order</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($serviceDetails as $service)
                    <tr>
                        <td>{{$tableCounter++}}</td>
                        <td>{{$service->user_type}}</td>
                        <td>{{array_key_exists($service->main_category_id,$mainCategories) == true ? $mainCategories[$service->main_category_id] : '-'}}</td>
                        <td>{{array_key_exists($service->sub_category_id,$subCategory) == true ? $subCategory[$service->sub_category_id] : '-'}}</td>
                        <td>{{$service->price_per_fixed_amount}}</td>
                        <td>{{$service->min_order}} / {{$service->max_order}}</td>
                        <td>{{$service->status == 1 ? 'Enable' : 'Disable'}}</td>
                        <td>
                            <a href="{{url('/super/admin/service/edit/'.$service->id)}}" class="btn btn-success">View</a>
                            <a href="{{url('/super/admin/service/detete/'.$service->id)}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allorderSA').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endsection