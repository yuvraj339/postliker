@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'Service','levelOneLink'=>'/super/admin/','levelTwo'=>'Add','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="blank-page">
            {!! Form::model($serviceDetails,array('url'=>'super/admin/service/edit/'.$serviceDetails->id,'class' => 'form-horizontal','accept-charset'=>'UTF-8', 'enctype'=>'multipart/form-data')) !!}
                {{csrf_field()}}
                @include('superAdmin.service.SA-ServiceAddEditCommon')
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('javascript')
    @include('partial.subCategoriesJs')

@endsection
