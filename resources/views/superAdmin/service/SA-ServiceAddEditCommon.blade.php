@include('partial.userMainCateSubCate',['type'=>'service'])
{{--<div class="form-group">
    <label class="col-sm-2 control-label">Service Id</label>
    <div class="col-sm-8">
        {{ Form::input('text', 'service_id',null,['class'=>'form-control1']) }}

    </div>
</div>--}}

{{--
<div class="form-group">
    <label class="col-sm-2 control-label">Brand</label>
    <div class="col-sm-8">
        {{ Form::input('text', 'brand',null,['class'=>'form-control1']) }}

    </div>
</div>
--}}

{{--<div class="form-group">
    <label class="col-sm-2 control-label">Service Name</label>
    <div class="col-sm-8">
        {{ Form::input('text', 'service_name',null,['class'=>'form-control1','id'=>'serviceName']) }}
    </div>
    --}}{{--<div class="col-sm-2">
        <p class="help-block">Your help text!</p>
    </div>--}}{{--
</div>--}}

<div class="form-group">
    <label class="col-sm-2 control-label">Price Per 1000
        <small> (INR)</small>
    </label>
    <div class="col-sm-8">
        {{ Form::input('text', 'price_per_fixed_amount',null,['class'=>'form-control1']) }}
    </div>
    {{--<div class="col-sm-2">
        <p class="help-block">Your help text!</p>
    </div>--}}
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Min Order</label>
    <div class="col-sm-8">
        {{ Form::input('number', 'min_order',null,['class'=>'form-control1']) }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Max Order</label>
    <div class="col-sm-8">
        {{ Form::input('number', 'max_order',null,['class'=>'form-control1']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Details</label>
    <div class="col-sm-8">
        {{ Form::textarea('details',null,['class'=>'form-control1']) }}
    </div>
</div>

<div class="form-group">
    <label for="selector1" class="col-sm-2 control-label">Status</label>
    <div class="col-sm-8">
        {{ Form::select('status', [
                    '1' => 'Enable',
                    '0' => 'Disable'],null,['class'=>'form-control1']
                 ) }}
    </div>
</div>

<div class="panel-footer">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <button class="btn-primary btn" type="submit">Submit</button>
            <button class="btn-inverse btn" type="reset">Reset</button>
        </div>
    </div>
</div>

