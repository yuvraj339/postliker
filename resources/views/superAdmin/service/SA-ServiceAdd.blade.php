@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'Service','levelOneLink'=>'/super/admin/','levelTwo'=>'Add','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="blank-page">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{csrf_field()}}
                @include('superAdmin.service.SA-ServiceAddEditCommon')
            </form>
        </div>
    </div>

@endsection

@section('javascript')
    @include('partial.subCategoriesJs')
@endsection
