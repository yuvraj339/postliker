@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'order','levelOneLink'=>'/super/admin/','levelTwo'=>'View','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')

        <div class="blank-page">
            <table id="allorderSA" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Message</th>
                </tr>
                </thead>

                <tbody>
                @foreach($contactDetails as $contact)
                    <tr>
                        <td>{{$tableCounter++}}</td>
                        <td>{{$contact->name}}</td>
                        <td>{{$contact->email}}</td>
                        <td>{{$contact->mobile}}</td>
                        <td>{{$contact->message}}</td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allorderSA').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endsection