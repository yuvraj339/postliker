@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'User','levelOneLink'=>'/super/admin/','levelTwo'=>'View','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')

        <div class="blank-page">
            <table id="allUsersSA" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>User Type</th>
                    <th>Total Fund</th>
                    <th>Contact No</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($userDetails as $user)
                    <tr>
                        <td>{{ucwords($user->name)}}</td>
                        <td>{{ucfirst($user->user_type)}}</td>
                        <td>{{ucfirst($user->total_fund)}}</td>
                        <td>{{ucfirst($user->contact_no)}}</td>
                        <td>{{$user->user_status == 1 ? 'Enable' : 'Disable'}}</td>
                        <td>
                            <a href="{{url('super/admin/user/edit/'.$user->id)}}" class="btn btn-success">View</a>
                            <a href="{{url('super/admin/user/delete/'.$user->id)}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allUsersSA').DataTable( {
                responsive: true,
                "scrollX": true
            } );
        });
    </script>
@endsection