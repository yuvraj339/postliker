@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'User','levelOneLink'=>'/super/admin/','levelTwo'=>'User Group','levelTwoLink'=>null])

    <div class="blank">
        <div class="blank-page">
            <table id="allUsersGroupSA" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>id</th>
                    <th>User Type</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Super Admin</td>
                        <td>
                            <a href="{{url('/')}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Retailer</td>
                        <td>
                            <a href="{{url('/')}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>General</td>
                        <td>
                            <a href="{{url('/')}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allUsersGroupSA').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endsection