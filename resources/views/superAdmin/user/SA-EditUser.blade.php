@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'View user','levelOneLink'=>'/super/admin/user/view','levelTwo'=>'Edit','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="">

            <div class="grid-form1">
                <div class="tab-content">
                    <div class="tab-pane active" id="horizontal-form">
                        {!! Form::model($userDetails,array('url'=>'super/admin/user/edit/'.$userDetails->id,'class' => 'form-horizontal','accept-charset'=>'UTF-8', 'enctype'=>'multipart/form-data')) !!}
                        {{csrf_field()}}

                        @include('superAdmin.user.SA-AddEditUserCommon',['mode'=>'edit','getId'=>$userDetails->id])

                        <div class="form-group">
                            <label for="selector1" class="col-sm-2 control-label">Profile Picture</label>
                            <div class="col-sm-8">

                                {{ Form::image($userDetails->profile_img != null ? url('/uploads/profileImg/'.$userDetails->profile_img): 'img/user-dummy-pic.png', 'profile_img', ['alt'=>'Profile picture','class' => 'img-circle img-responsive col-sm-3']) }}

                                {!! Form::file('profile_img',['id'=>"file"]) !!}
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button class="btn-primary btn" type="submit">Submit</button>
                                    <button class="btn-inverse btn" type="reset">Reset</button>
                                </div>
                            </div>
                        </div>

                        {{Form::close()}}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection