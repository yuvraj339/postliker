@if($getId != Auth::user()->id)
    <div class="form-group">
        <label for="selector1" class="col-sm-2 control-label">User Type</label>
        <div class="col-sm-8">
            {{ Form::select('user_type', [
                        'general' => 'General',
                        'retailer' => 'Retailer'],null,['class'=>'form-control1']
                     ) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Full Name</label>
        <div class="col-sm-8">
            {{ Form::input('text', 'name',null,['class'=>'form-control1']) }}
        </div>
        {{--<div class="col-sm-2">
            <p class="help-block">Your help text!</p>
        </div>--}}
    </div>
    @else
    <div class="form-group">
        <label class="col-sm-2 control-label">API KEY</label>
        <div class="col-sm-8">
            {{ Form::input('text', 'api_key',null,['class'=>'form-control1']) }}
        </div>
        {{--<div class="col-sm-2">
            <p class="help-block">Your help text!</p>
        </div>--}}
    </div>

@endif
<div class="form-group">
    <label class="col-sm-2 control-label">Email</label>
    <div class="col-sm-8">
        {{ Form::input('email', 'email',null,['class'=>'form-control1']) }}
        @if($errors->has('email'))<span class="text-error">  {{ $errors->first('email') }}   </span>@endif

    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Total Fund</label>
    <div class="col-sm-8">
        {{ Form::input('text', 'total_fund',null,['class'=>'form-control1']) }}
    </div>
</div>

<div class="form-group">
    <label for="selector1" class="col-sm-2 control-label">Gender</label>
    <div class="col-sm-8">
        {{ Form::select('gender', [
                    'male' => 'Male',
                    'female' => 'Female'],null,['class'=>'form-control1']
                 ) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Skype Id</label>
    <div class="col-sm-8">
        {{ Form::input('text', 'skype_id',null,['class'=>'form-control1']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Address</label>
    <div class="col-sm-8">
        {{ Form::textarea('address',null,['class'=>'form-control1']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Contact No 1</label>
    <div class="col-sm-8">
        {{ Form::input('text', 'contact_no',null,['class'=>'form-control1']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Contact No 2</label>
    <div class="col-sm-8">
        {{ Form::input('text', 'alternative_contact_no',null,['class'=>'form-control1']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Password</label>
    <div class="col-sm-8">
        {{ Form::input('password', 'password',null,['class'=>'form-control1']) }}
        @if($mode == 'edit')
            <p class="text-warning">Leave blank if you don't want update password.</p>
        @endif
    </div>
    {{--<div class="col-sm-2">
        <p class="help-block">Your help text!</p>
    </div>--}}
</div>

<div class="form-group">
    <label for="selector1" class="col-sm-2 control-label">User Status</label>
    <div class="col-sm-8">
        {{ Form::select('user_status', [
                    '1' => 'Enable',
                    '0' => 'Disable'],null,['class'=>'form-control1']
                 ) }}
    </div>
</div>
