@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'User','levelOneLink'=>'/super/admin/','levelTwo'=>'Create','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="">

            <div class="grid-form1">
                <div class="tab-content">
                    <div class="tab-pane active" id="horizontal-form">
                        <form class="form-horizontal" role="form" method="POST" action="">
                        {{csrf_field()}}

                            @include('superAdmin.user.SA-AddEditUserCommon',['mode'=>'add','getId'=>0])

                            <div class="form-group">
                                <label for="selector1" class="col-sm-2 control-label">Profile Picture</label>
                                <div class="col-sm-8">

                                    {{--{{ Form::image('img/user-dummy-pic.png', 'profile_img', ['alt'=>'Profile picture','class' => 'img-circle img-responsive']) }}--}}

                                    {!! Form::file('profile_img',['id'=>"file"]) !!}
                                </div>
                            </div>

                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <button class="btn-primary btn" type="submit">Submit</button>
                                        <button class="btn-inverse btn" type="reset">Reset</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

@endsection