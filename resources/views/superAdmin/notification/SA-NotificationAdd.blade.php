@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'Service','levelOneLink'=>'/super/admin/','levelTwo'=>'Add','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')
        <div class="blank-page">
            <form class="form-horizontal" role="form" method="POST" action="">
                {{csrf_field()}}
                {{--<div class="form-group">
                    <label class="col-sm-2 control-label">Select User</label>
                        <div class="col-sm-8">
                            {{ Form::select('user_id',$userList,null,['class'=>'form-control1']) }}
                        </div>
                </div>--}}

                <div class="form-group">
                    <label class="col-sm-2 control-label">User Group</label>
                    <div class="col-sm-8">
                            {{ Form::select('user_type', [
                                        'retailer' => 'Retailer',
                                        'general' => 'General'],null,['class'=>'form-control1']
                                     ) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Heading</label>
                    <div class="col-sm-8">
                        {{ Form::input('text', 'user_notification_type',null,['class'=>'form-control1']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Notification</label>
                    <div class="col-sm-8">
                        {{ Form::textarea('notification_details',null,['class'=>'form-control1']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="selector1" class="col-sm-2 control-label">Global</label>
                    <div class="col-sm-8">
                        {{ Form::select('global_notification', [
                                    '0' => 'No',
                                    '1' => 'Yes'],null,['class'=>'form-control1']
                                 ) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="selector1" class="col-sm-2 control-label">Global</label>
                    <div class="col-sm-8">
                        {{ Form::select('status', [
                                    '1' => 'Enable',
                                    '0' => 'Disable'],null,['class'=>'form-control1']
                                 ) }}
                    </div>
                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn-primary btn" type="submit">Submit</button>
                            <button class="btn-inverse btn" type="reset">Reset</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
