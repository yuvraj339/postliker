@extends('layouts.SA-Layout')
@section('content')
    @include('superAdmin.partial.breadcrumbs',['levelOne'=>'order','levelOneLink'=>'/super/admin/','levelTwo'=>'View','levelTwoLink'=>null])

    <div class="blank">
        @include('partial.alert')

        <div class="blank-page">
            <table id="allorderSA" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>Notify By</th>
                    <th>User Group</th>
                    <th>Is Global</th>
                    <th>Notification Type</th>
                    <th>notification_details</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($notificationDetails as $notification)
                    <tr>
                        <td>{{$tableCounter++}}</td>
                        <td>{{ucwords($userDetails[$notification->user_id])}}</td>
                        <td>{{ucwords($notification->user_type) }}</td>
                        <td>{{$notification->global_notification == 1 ? 'Yes' :' No'}}</td>
                        <td>{{$notification->user_notification_type}}</td>
                        <td>{{$notification->notification_details}}</td>
                        <td>{{$notification->status == 1 ? 'Enable' : 'Disable'}}</td>
                        <td>
{{--                            <a href="{{url('/super/admin/notification/edit/'.$notification->id)}}" class="btn btn-success">View</a>--}}
                            <a href="{{url('/super/admin/notification/detete/'.$notification->id)}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allorderSA').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endsection