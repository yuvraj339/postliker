@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="banner-text">
            <section class="cd-intro">
                <h2 class="cd-headline letters scale">
                    <span>Greetings of the day</span>
                    <span class="cd-words-wrapper">
				{{--<b class="is-visible">{{ config('app.name', 'Liker') }}</b>--}}
				{{--<b style="">{{ucwords('We are an internet marketing company')}} </b>--}}
				{{--<b>Offering services relating to social media marketing</b>--}}
				{{--<b> We are one of the best in our field </b>--}}
			</span>
                </h2>
            </section>
            <div class="botton-agileits">
                <a href="#" class="hvr-rectangle-in" data-toggle="modal" data-target="#myModal1">know more</a>
            </div>
            <div class="agileits_w3layouts_call_mail">
                <ul>
                    <li><i class="fa fa-phone" aria-hidden="true"></i>(+91) 9967544889</li>
                    <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a
                                href="mailto:mdhemantsharma@gmail.com">mdhemantsharma@gmail.com</a></li>
                </ul>
            </div>
            {{--<div class="banner-icons-agileinfo">--}}
                {{--<ul class="agileits_social_list">--}}
                    {{--<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>--}}
                    {{--<li><a href="#" class=" fa fa-instagram"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>--}}
                    {{--<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>--}}
                    {{--<li><a href="#" class="w3_agile_instagram"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>
    </div>
    </div>
    <!--banner end here-->

    <!-- Modal1 -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>{{ config('app.name', 'Liker') }}</h4>
                    <img src="{{url('/img/bg3.png')}}" alt=" " class="img-responsive">
                    <h3 class="subheading-w3l">Welcome to <span>{{ config('app.name', 'Liker') }}</span> </h3>
                    <p>We are pleased to introduce ourselves as a social media marketing and promotions company. We cover
                        all the possible ways of effective online marketing and promotions. We target on creating awareness and buzz around your brand, service or product.<br>

                        In the recent years, social media has taken the world with a boom. It’s the most effective way to
                        show you or your business in the best light. It’s all about how you portray yourself or your
                        products in the most efficient way to gather attention and audience.<br>

                        Using our experience and lessons learned, we can help boost you/your brand on the social media
                        platform. We work on putting extra effort into your profile management to WOW your online presence.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- //Modal1 -->

    <!-- banner-bottom -->
    <div id="about" class="banner-bottom">
        <div class="container">
            <div class="tittle-agileinfo">
                <h3>About Us</h3>
            </div>
            <div class="w3ls_banner_bottom_grid1">
                <div class="col-md-6 w3l_banner_bottom_left">
                    <img src="{{url('/img/bg3.png')}}" alt=" " class="img-responsive"/>
                </div>
                <div class="col-md-6 w3l_banner_bottom_right">
                    <h3 class="subheading-w3l">Welcome to <span>{{ config('app.name', 'Liker') }}</span></h3>
                    <p>We are pleased to introduce ourselves as a social media marketing and promotions company. We cover
                        all the possible ways of effective online marketing and promotions. We target on creating awareness and buzz around your brand, service or product.<br>

                        In the recent years, social media has taken the world with a boom. It’s the most effective way to
                        show you or your business in the best light. It’s all about how you portray yourself or your
                        products in the most efficient way to gather attention and audience.<br>

                        Using our experience and lessons learned, we can help boost you/your brand on the social media
                        platform. We work on putting extra effort into your profile management to WOW your online presence.</p>
                    <ul>
                        <li><i class="fa fa-clone" aria-hidden="true"></i>Facebook</li>
                        <li><i class="fa fa-clone" aria-hidden="true"></i>Youtube</li>
                        <li><i class="fa fa-clone" aria-hidden="true"></i>Instagram</li>
                        <li><i class="fa fa-clone" aria-hidden="true"></i>Twitter</li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //banner-bottom -->
    <!--latest-works-->
{{--
    <div class="latest-works" id="works">
        <div class="container">
            <div class="tittle-agileinfo">
                <h3 class="white-w3ls">Latest Works</h3>
            </div>
            <div class="col-md-8 agileits_banner_bottom_grid_three">
                <div class="agileinfo_banner_bottom_grid_three_left">
                    <div class="wthree_banner_bottom_grid_three_left1 grid">
                        <figure class="effect-roxy">
                            <img src="{{url('/')}}/homepage/images/a2.jpg" alt=" " class="img-responsive"/>
                            <figcaption>
                                <h3><span>M</span>ending</h3>
                                <p>Vestibulum pulvinar lobortis lorem lectus pretium non.</p>
                            </figcaption>
                        </figure>
                    </div>
                    <p class="w3_agileits_para">Pellentesque vehicula augue eget nisl ullamcorper,
                        molestie blandit ipsum auctor. Mauris volutpat augue dolor.</p>
                </div>
                <div class="agileinfo_banner_bottom_grid_three_left">
                    <p class="w3_agileits_para">Pellentesque vehicula augue eget nisl ullamcorper,
                        molestie blandit ipsum auctor. Mauris volutpat augue dolor.</p>
                    <div class="wthree_banner_bottom_grid_three_left1 grid">
                        <figure class="effect-roxy">
                            <img src="{{url('/')}}/homepage/images/a3.jpg" alt=" " class="img-responsive"/>
                            <figcaption>
                                <h3><span>D</span>esigning</h3>
                                <p>Vestibulum pulvinar lobortis lorem lectus pretium non.</p>
                            </figcaption>
                        </figure>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-4 agileinfo_banner_bottom_grid_three_left bnr-btm3">
                <div class="wthree_banner_bottom_grid_three_left1 grid">
                    <figure class="effect-roxy">
                        <img src="{{url('/')}}/homepage/images/a4.jpg" alt=" " class="img-responsive"/>
                        <figcaption>
                            <h3><span>A</span>lteration</h3>
                            <p>Vestibulum pulvinar lobortis lorem lectus pretium non.</p>
                        </figcaption>
                    </figure>
                </div>
                <p class="w3_agileits_para">Pellentesque vehicula augue eget nisl ullamcorper,
                    molestie blandit ipsum auctor. Mauris volutpat augue dolor.</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
--}}
    <!--//Latest works-->
    <!-- services -->
    <div class="services" id="services">
        <div class="container">
            <div class="tittle-agileinfo">
                <h3>Featured Services</h3>
            </div>
            <div class="w3_agileits_services_grids">
                <div class="w3_agileits_services_left">
                    <img src="{{url('/')}}/homepage/images/sociallike.jpg   " alt=""/>
                </div>
                <div class="w3_agileits_services_right">
                    <div class="col-md-6 w3_agileits_services_grid">
                        <div class="w3_agileits_services_grid_agile">
                            <div class="w3_agileits_services_grid_1">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </div>
                            <h3>Facebook</h3>
                            <p> <span>
                                    - Fan Page Likes/Fans<br>
                                    - Worldwide facebook fans (likes)<br>
                                    - Indian Facebook Fans(Targeted Indian audience)<br>
                                    - United states,turkey,russian,asian,australian &amp; more {{--facebook fans<br>--}}
                                    {{--- Facebook Followers (Worldwide/Indian)<br>
                                    - Facebook Page Verification (Verified facebook page)<br>
                                    - Facebook Page Management<br>
                                    - Movie Promotion--}}
                                </span></p>
                        </div>
                    </div>
                    <div class="col-md-6 w3_agileits_services_grid">
                        <div class="w3_agileits_services_grid_agile">
                            <div class="w3_agileits_services_grid_1">
                                <i class="fa fa-youtube" aria-hidden="true"></i>
                            </div>
                            <h3>Youtube</h3>
                            <p>
                                 <span>
                                    -Views<br>
                                    -Worldwide<br>
                                    -Country Targeted (usa,india,russia or more)<br>
                                    -Likes/comment/subscribers<br>
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 w3_agileits_services_grid">
                        <div class="w3_agileits_services_grid_agile">
                            <div class="w3_agileits_services_grid_1">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </div>
                            <h3>Twitter</h3>
                            <p> <span>
                                    -Twitter followers<br>
                                    -Worldwide <br>
                                    -Country targeted (indian,usa,arab,Russia)<br>
                                    -Twitter retweets/Favorites/trend<br>
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 w3_agileits_services_grid">
                        <div class="w3_agileits_services_grid_agile">
                            <div class="w3_agileits_services_grid_1">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </div>
                            <h3>Instagram</h3>
                            <p>
                                 <span>
                                    -Followers (worldwide/country targeted)<br>
                                    -Likes
                                </span>
                            </p>
                        </div>
                    </div>

                    {{-- <div class="col-md-6 w3_agileits_services_grid">
                         <div class="w3_agileits_services_grid_agile">
                             <div class="w3_agileits_services_grid_1">
                                 <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                             </div>
                             <h3>{{ config('app.name', 'Liker') }}</h3>
                             <p>Donec semper rutrum ipsum et bibendum. Sed condimentum dolor velit.</p>
                         </div>
                     </div>
                    <div class="col-md-6 w3_agileits_services_grid">
                          <div class="w3_agileits_services_grid_agile">
                              <div class="w3_agileits_services_grid_1">
                                  <i class="fa fa-link" aria-hidden="true"></i>
                              </div>
                              <h3>Mending</h3>
                              <p>Donec semper rutrum ipsum et bibendum. Sed condimentum dolor velit.</p>
                          </div>
                      </div>--}}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- //services -->
    <!-- about -->
    <div class="about-w3layouts">
        <div class="container">
            <div class="tittle-agileinfo">
                <h3 class="white-w3ls">Some Interesting Facts</h3>
            </div>
            <!-- schedule-bottom -->
            <div class="schedule-bottom">
                <div class="agileits_schedule_bottom_right">
                    <div class="w3ls_schedule_bottom_right_grid">
                        <h3 class="subheading-w3l white-w3ls">{{ config('app.name', 'Liker') }} an <span>internet marketing</span> company,offering services relating to <span>social media</span> marketing, <span>profile verification</span>
                            management.</h3>
                        <p>These aforementioned services will surely help you SHINE ONLINE- just as we always commit to and work hard to deliver the best results</p>
                        <div class="col-md-4 w3l_schedule_bottom_right_grid1">
                            <i class="fa fa-user-o" aria-hidden="true"></i>
                            <h4>Customers</h4>
                            <h5 class="counter">792</h5>
                        </div>
                        <div class="col-md-4 w3l_schedule_bottom_right_grid1">
                            <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            <h4>Services</h4>
                            <h5 class="counter">90</h5>
                        </div>
                        <div class="col-md-4 w3l_schedule_bottom_right_grid1">
                            <i class="fa fa-shield" aria-hidden="true"></i>
                            <h4>Awards</h4>
                            <h5 class="counter">95</h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- //schedule-bottom -->
        </div>
    </div>
    <!-- //about-bottom -->
    <!-- team -->
    <div class="team" id="experts">
        <div class="container">
            <div class="tittle-agileinfo">
                <h3>Meet our experts</h3>
            </div>
            <div class="w3ls_banner_bottom_grids">
                <div class="col-md-8 team-agileits-left">
                    <div class="w3_agile_team_grid">
                        <div class="w3layouts_team_grid team1">
                            <div class="w3layouts_team_grid_pos">
                                <div class="wthree_text">
                                    <ul class="agileits_social_list">
                                        <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                                        <li><a href="#" class="agile_twitter"><i class="fa fa-twitter"
                                                                                 aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="team1-info-w3ls">
                            <h6>Hemment Sharma (Hemu)</h6>
                            <span>Founder</span>
                            <p>Donec semper rutrum ipsum et bibendum.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="w3_agile_team_grid">
                        <div class="w3layouts_team_grid team2">
                            <div class="w3layouts_team_grid_pos">
                                <div class="wthree_text">
                                    <ul class="agileits_social_list">
                                        <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                                        <li><a href="#" class="agile_twitter"><i class="fa fa-twitter"
                                                                                 aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="team2-info-w3ls">
                            <h6>Hemu friend</h6>
                            <span>Co-Founder</span>
                            <p>Donec semper rutrum ipsum et bibendum.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4 team-agileits-right">
                    <div class="w3_agile_team_grid">
                        <div class="w3layouts_team_grid team3">
                            <div class="w3layouts_team_grid_pos">
                                <div class="wthree_text">
                                    <ul class="agileits_social_list">
                                        <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                                        <li><a href="#" class="agile_twitter"><i class="fa fa-twitter"
                                                                                 aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="team3-info-w3ls">
                            <h6>Yuvraj Singh (Developer / Designer)</h6>
                            <span>Designer</span>
                            <p>Donec semper rutrum ipsum et bibendum.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //team -->
    <!-- gallery -->

    <h6 class="contact-w3ls" id="contact">Call <span>09967544889</span> to make an appointment</h6>
    <div class="footer-w3layouts">
        <div class="container">
            <div class="col-md-4 footer-grids">
                <h3>Services</h3>
                <ul class="b-nav">
                    <li><a class="scroll" href="index.html">Home</a></li>
                    <li><a class="scroll" href="#about">About</a></li>
                    <li><a class="scroll" href="#Services">Service</a></li>
                    <li><a class="scroll" href="#experts">Experts</a></li>
                </ul>
            </div>
            <div class="col-md-4 footer-grids">
                <h3>About {{ config('app.name', 'Liker') }}</h3>
                <p>We are pleased to introduce ourselves as a social media developer and promotions company. We cover all the possible ways of effective online marketing and promotions. We target on creating awareness and buzz around your brand, service or product.</p>
                <div class="botton-agileits">
                    <a href="#" class="hvr-rectangle-in" data-toggle="modal" data-target="#myModal1">know more</a>
                </div>
               {{-- <ul class="agileits_social_list">
                    <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                </ul>--}}
            </div>
            <div class="col-md-4 footer-grids">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3522.105606625805!2d73.27833651506897!3d28.021247882663022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjjCsDAxJzE2LjUiTiA3M8KwMTYnNDkuOSJF!5e0!3m2!1sen!2s!4v1494167063675" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <!-- //contact -->
@endsection
