@extends('layouts.dashboard')

@section('dashboard')

    <div class="">

        <div class="row">
            @include('partial.alert')
            <div class="pull-right">
                {{--<p class=" text-info">Member since--}}
                {{--                    : {{$userDetails != '' ? \Carbon\Carbon::parse($userDetails->created_at)->toDayDateTimeString() : \Carbon\Carbon::now()->toDayDateTimeString()}} </p>--}}
                <br>
            </div>
            <div class="">
                {!! Form::model($userDetails,array('url'=>'dashboard/user/profile/update/'.$userDetails->id,'class' => 'form-horizontal','accept-charset'=>'UTF-8', 'enctype'=>'multipart/form-data')) !!}
                <div class="">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center">

{{--                                {{ Form::image('img/user-dummy-pic.png', 'profile_img', ['alt'=>'Profile picture','class' => 'img-circle img-responsive']) }}--}}
                                {{ Form::image($userDetails->profile_img != null ? url('/uploads/profileImg/'.$userDetails->profile_img): 'img/user-dummy-pic.png', 'profile_img', ['alt'=>'Profile picture','class' => 'img-circle img-responsive']) }}

                                <div class="custom-file-upload">
                                     {!! Form::file('profile_img_upload',['id'=>"file"]) !!}
                                </div>
                            </div>


                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td class=" col-md-3 ">Full Name:</td>
                                        <td>{{ Form::input('text', 'name',null,['class'=>'form-control col-md-9 input-sm ']) }}</td>
                                        {{--<td> {{$userDetails != '' ? $userDetails->name : '-'}}</td>--}}
                                    </tr>
                                   {{-- <tr>
                                        <td class=" col-md-3 ">Current Amount:</td>
                                        <td>{{ Form::input('text', 'total_fund',null,['class'=>'form-control col-md-9 input-sm ']) }}</td>

                                        --}}{{--<td> {{$userDetails != '' ? $userDetails->total_fund : '0'}}</td>--}}{{--
                                    </tr>--}}
                                    <tr>
                                        <td class=" col-md-3 ">{{ Form::select('skype_id', [
                                                                'skype' => 'Skype Id',
                                                                'whatsapp' => 'Whatsapp No',
                                                                'phone' => 'Phone No'
                                                                ],null,['class'=>'form-control']
                                                             ) }}
                                            @if ($errors->has('skype_id'))
                                                <span class="help-block">
                                        <strong>Please select a contact type.</strong>
                                    </span>
                                            @endif</td>
                                    <td>{{ Form::input('text', 'alternative_contact_no',null,['class'=>'form-control col-md-9 input-sm ']) }}</td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td class=" col-md-3 ">Gender:</td>
                                        <td>
                                            {{ Form::select('gender', [
                                               'male' => 'Male',
                                               'female' => 'Female'],null,['class'=>'form-control col-md-9 input-sm ']
                                            ) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class=" col-md-3">Contact No:</td>

                                        <td class="form-group {{$errors->has('contact_no') ? 'has-error' : ''}}">{{ Form::input('text', 'contact_no',null,['class'=>'form-control col-md-4 input-sm ']) }}
                                            @if($errors->has('contact_no'))<span class=" text-danger" >  {{ $errors->first('contact_no') }}   </span>@endif
                                        </td>

                                    </tr>
                                    {{--<tr>--}}
                                        {{--<td class=" col-md-3 ">Contact No 2:</td>--}}
                                        {{--<td>{{ Form::input('text', 'alternative_contact_no',null,['class'=>'form-control col-md-9 input-sm ']) }}</td>--}}
                                    {{--</tr>--}}
                                    <tr>
                                        <td class=" col-md-3 ">Email:</td>
                                        <td class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                                            {{ Form::input('email', 'email',null,['class'=>"form-control col-md-9 input-sm "]) }}
                                            @if($errors->has('email'))<span class="text-error" >  {{ $errors->first('email') }}   </span>@endif

                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{url('/dashboard/contact-us')}}" data-original-title="Broadcast Message" data-toggle="tooltip" type="button"
                           class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                        <span class="pull-right">
                            <button type="submit" class="btn btn-success btn-sm"data-original-title="Edit this user"
                                    data-toggle="Update">Update</button>
                            {!! Form::reset('Reset', ['class' => 'btn btn-danger btn-sm   ']) !!}
                            {{--<a href="{{url('/dashboard/user/profile/edit')}}" data-original-title="Edit this user"
                               data-toggle="tooltip" type="button"
                               class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button"
                               class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>--}}
                        </span>
                    </div>

                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection