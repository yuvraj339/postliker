@extends('layouts.dashboard')

@section('dashboard')
    <table id="accountSection" class="display" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th>S.N</th>
            <th>User Name</th>
            <th>Credited / Debited By</th>
            <th>Amount</th>
            <th>Credit/Debit</th>
            <th>Date</th>
        </tr>

        </thead>

        <tbody>
        @foreach($userAccountDetails as $accountInfo)

            <tr>
                <td>{{$tableCounter++}}</td>
                <td>{{$userDetails[$accountInfo->user_id]}}</td>
                <td>{{$userDetails[$accountInfo->fund_added_by]}}</td>
                <td>{{$accountInfo->fund_credit_debit_value}}</td>
                <td>{{$accountInfo->fund_credit_status == 1 ? 'Credited' : 'Debited'}}</td>
                <td>{{$accountInfo->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#accountSection').DataTable();
        });
    </script>
@endsection
