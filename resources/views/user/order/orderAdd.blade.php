@extends('layouts.dashboard')

@section('dashboard')
    <div class="">

        <div class="row">
            @include('partial.alert')
            <form class="form-horizontal" role="form" method="POST" action="">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="selector1" class="col-sm-2 control-label">Your Name</label>
                    <div class="col-sm-8">
                        {{ Form::input('text', null,Auth::user()->name,['class'=>'form-control input-sm ','readonly']) }}

                    </div>
                </div>
                @include('partial.userMainCateSubCate',['type' => 'userOrder'])

                <div class="form-group">
                    <label class="col-sm-2 control-label">Enter URL<b class="text-danger">*</b> </label>
                    <div class="col-sm-8">
                        {{ Form::input('text', 'order_on_url',null,['class'=>'form-control input-sm ']) }}
                        @if ($errors->has('order_on_url'))
                            <span class="text-danger"> {{ $errors->first('order_on_url') }} </span>
                        @endif
                    </div>
                    {{--<div class="col-sm-2">
                        <p class="help-block">Your help text!</p>
                    </div>--}}
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Enter Amount<b class="text-danger">*</b>
                        {{--<small> (INR)</small>--}}
                    </label>
                    <div class="col-sm-8">
                        {{ Form::input('text', 'amount',null,['class'=>'form-control input-sm ','autocomplete'=>'off','id'=>'getAmount','onkeyup'=>'calculateTotalAmount()']) }}
                        @if ($errors->has('amount'))
                            <span class="text-danger"> {{ $errors->first('amount') }} </span>
                        @endif
                    </div>
                    {{--<div class="col-sm-2">--}}
                        {{--<p class="help-block" id="putTotalAmount"></p>--}}
                    {{--</div>--}}
                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;
                    </label>
                    <div class="col-sm-8">
                        <p class="help-block" id="putTotalAmount"></p>
                    </div>
                </div>

                <div class="panel-footer" style="vertical-align: bottom">
                    <a href="{{url('/dashboard/contact-us')}}" data-original-title="Broadcast Message" data-toggle="tooltip" type="button"
                       class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                    <span class="pull-right">
                <button type="submit" class="btn btn-success btn-sm" data-original-title="Edit this user"
                        data-toggle="Submit">submit</button>
                <input class="btn btn-danger btn-sm   " type="reset" value="Reset">

            </span>
                </div>

            </form>
        </div>
    </div>

@endsection
@section('javascript')
@include('partial.subCategoriesJs')
@endsection

