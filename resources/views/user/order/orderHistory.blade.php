@extends('layouts.dashboard')

@section('dashboard')
    <table id="orderSection" class="display" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th>Order Id</th>
            <th>Service Name</th>
            <th>Amount</th>
            <th>Order on URL</th>
            <th>Date</th>
            <th>S/C/R/F</th>
            <th>Status</th>
        </tr>
        </thead>

        <tbody>
        @foreach($orderDetails as $order)
            <tr>
                <td>GP-{{$order->id}}</td>
                <td>{{$serviceDetails[$order->sub_category_id]}}</td>
                <td>{{$order->amount}}</td>

                <td>{{$order->order_on_url}}</td>
                <td>{{$order->created_at}}</td>
                <td>
                    {{$order->start_count}} /
                    {{ $order->status == 'Completed' ? (int)$order->start_count + (int)$order->amount : 0 }} /
                    0 /
                    {{(int)$order->start_count + (int)$order->amount}}
                </td>
                <td>
                    <i class="glyphicon glyphicon-refresh"
                       style="cursor: pointer"
                       onclick="recheckStatus('{{$order->order_id}}','{{$order->id}}')">
                    </i>

                    <p id="updateStatus{{$order->id}}">
                        {{$order->status }}
                    </p>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#orderSection').DataTable();
        });
    </script>


    <script type="text/javascript">
        function recheckStatus(orderId, id) {
            var base_url = window.location.origin;
            $.ajax({
                type: "get",
                url: base_url + "/order/" + id + "/status/" + orderId,
                dataType: 'json',
                async: false, //This is deprecated in the latest version of jquery must use now callbacks
                success: function (jsonData) {
                    alert(jsonData.orderStatus);
                    document.getElementById("updateStatus"+id).innerHTML = jsonData.orderStatus;
                }
            });
        }
    </script>
@endsection
