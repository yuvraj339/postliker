@extends('layouts.dashboard')

@section('dashboard')

    @include('partial.alert')

    <table id="allorderSA" class="display" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th>S.N</th>
            {{--<th>Notify By</th>--}}
            {{--<th>User Group</th>--}}
            {{--<th>Is Global</th>--}}
            <th>Notification Type</th>
            <th>notification_details</th>
            <th>date</th>
            {{--<th>Action</th>--}}
        </tr>
        </thead>

        <tbody>
        @foreach($notificationDetails as $notification)
            <tr>
                <td>{{$tableCounter++}}</td>
{{--                <td>{{ucwords($notification->user_type) }}</td>--}}
                <td>{{$notification->user_notification_type}}</td>
                <td>{{$notification->notification_details}}</td>
                <td>{{$notification->created_at}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allorderSA').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endsection

