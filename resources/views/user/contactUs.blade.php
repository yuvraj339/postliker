@extends('layouts.dashboard')

@section('dashboard')
    <div class="">
        @include('partial.alert')
        <form class="form-horizontal" role="form" method="POST" action="">
            {{csrf_field()}}
            <div class="container-contact">
                <div class="head-contact">
                    <h5>Feel free to ask your queries?</h5>
                </div>
                <input type="text" class="form-control" value="{{auth::user()->name}}" name="name" placeholder="Name"/><br/>
                <input type="email" class="form-control" value="{{auth::user()->email}}" name="email" placeholder="Email"/><br/>
                <input type="text" class="form-control" value="{{auth::user()->contact_no}}" name="mobile" placeholder="Mobile"/><br/>
                <textarea type="text" class="form-control" name="message" placeholder="Message"></textarea><br/>
                <div class="message-contact"></div>
                <button id="submit" class="form-control btn btn-success" type="submit">
                    Send!
                </button>
            </div>
        </form>
    </div>

@endsection
