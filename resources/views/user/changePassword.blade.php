@extends('layouts.dashboard')

@section('dashboard')
    <div class="row">
        @include('partial.alert')

        <div class="well well-sm">
            “Change your passwords regularly” is a common piece of password advice, but it isn’t necessarily good
            advice.
            You shouldn’t bother changing most passwords regularly — it encourages you to use weaker passwords and
            wastes your time.
        </div>
        <form class="form-horizontal" role="form" method="POST"
              action="{{ url('/dashboard/change/password/'.Auth::user()->id) }}">
            {{csrf_field()}}
            <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                <label for="old_password" class="col-md-4 control-label">Password</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control input-sm" name="old_password" required>

                    @if ($errors->has('old_password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Password</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control input-sm" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control input-sm"
                           name="password_confirmation"
                           required>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Update
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection