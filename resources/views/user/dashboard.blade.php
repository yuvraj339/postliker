@extends('layouts.dashboard')

@section('dashboard')

    <div class="alert alert-dismissible alert-{{Auth::user()->total_fund > 1 ? 'success' : 'danger'}}">
        {{--<div class="well well-sm">--}}
        Welcome you {{ucwords(Auth::user()->name)}} your current account balance is {{Auth::user()->total_fund}}
        <a href="{{url('/dashboard/contact-us')}}" data-original-title="Broadcast Message" data-toggle="tooltip"
           type="button"
           class="btn btn-xs btn-primary pull-right"><i class="glyphicon glyphicon-envelope"></i></a>
        {{--</div>--}}
        <div id="source-button" class="btn btn-primary btn-xs" style="display: none;">&lt; &gt;</div>
    </div>

    <div class="alert alert-dismissible alert-success">
        <a href="{{url('/dashboard/notification')}}" class="btn btn-xs btn-primary pull-right" data-dismiss="alert">View
            All</a>
        <h4>Recent Notifications</h4>
        @foreach($notificationDetails as $notification)
            <p>{{$tableCounter++}} >> {{$notification->user_notification_type}} : {{$notification->notification_details}}</p>
        @endforeach
        <span class="hidden">{{$tableCounter = 1}}</span>
    </div>

    <div class="alert alert-dismissible alert-success">
        <a href="{{url('/dashboard/order/history')}}" class="btn btn-xs btn-primary pull-right" data-dismiss="alert">View
            All</a>
        <h4>Recent Orders</h4>
        @foreach($orderDetails as $order)
            <p>{{$tableCounter++}} >> URL : {{$order->order_on_url}} | Amount : {{$order->amount}}</p>
        @endforeach
        <span class="hidden">{{$tableCounter = 1}}</span>

    </div>

    <div class="alert alert-dismissible alert-success">
        <a href="{{url('/dashboard/account/setting')}}" class="btn btn-xs btn-primary pull-right" data-dismiss="alert">View
            All</a>
        <h4>Recent Account Transaction </h4>
        @foreach($userAccountDetails as $account)
            <p>{{$tableCounter++}} >> Your account has been {{$account->fund_credit_status == 1 ? 'credited' : 'debited'}} by {{$account->fund_credit_debit_value}} </p>
    @endforeach
@endsection
