@extends('layouts.dashboard')

@section('dashboard')
    <table id="accountSection" class="display" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th>S.N</th>
            <th>Brand</th>
            <th>Service Name</th>
            <th>Price On 1000</th>
            <th>Min/Max Order</th>
            <th>Details</th>
        </tr>

        </thead>

        <tbody>
        @foreach($serviceDetails as $service)
            <tr>
                <td>{{$tableCounter++}}</td>
                <td>{{array_key_exists($service->main_category_id,$mainCategories) == true ? $mainCategories[$service->main_category_id] : '-'}}</td>
                <td>{{array_key_exists($service->sub_category_id,$subCategory) == true ? $subCategory[$service->sub_category_id] : '-'}}</td>
                <td>
                    {{array_key_exists($service->sub_category_id,$priceRule) == true ? $priceRule[$service->sub_category_id] :  $service->price_per_fixed_amount }}
                </td>
                <td>{{$service->min_order}} / {{$service->max_order}}</td>
                <td>{{$service->details}} </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#accountSection').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endsection