    @extends('layouts.dashboard')

@section('dashboard')
    <div class="">
        <div class="row">
            {{--<div class="col-md-3 col-lg-3 ">--}}
            {{--&nbsp;--}}
            {{--</div>--}}
            <div class=" pull-right">
                {{--<A href="edit.html">Edit Profile</A>--}}

                {{--<A href="edit.html">Logout</A>--}}
                {{--<p class=" text-info">May 05,2014,03:00 pm </p>--}}
                <span class="text-info">Member since
                    : {{$userDetails != false ? \Carbon\Carbon::parse($userDetails['created_at'])->toDayDateTimeString() : \Carbon\Carbon::now()->toDayDateTimeString()}} </span>

            </div>
        </div>
        <div class="row">

            <div class="">
                {{--<div class="panel-heading">
                    <h3 class="panel-title">Sheena Shrestha</h3>
                </div>--}}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center">
                            {{--                                {{ Form::image(Auth::user()->profile_img != null ? url('/uploads/profileImg/'.Auth::user()->profile_img): 'img/user-dummy-pic.png', 'profile_img', ['alt'=>'Profile picture','class' => 'img-circle img-responsive']) }}--}}

                            <img alt="User Pic"
                                 src="{{Auth::user()->profile_img != null ? url('/uploads/profileImg/'.Auth::user()->profile_img): url('img/user-dummy-pic.png')}}"
                                 class="img-circle img-responsive"></div>

                        {{--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                          <dl>
                            <dt>DEPARTMENT:</dt>
                            <dd>Administrator</dd>
                            <dt>HIRE DATE</dt>
                            <dd>11/12/2013</dd>
                            <dt>DATE OF BIRTH</dt>
                               <dd>11/12/2013</dd>
                            <dt>GENDER</dt>
                            <dd>Male</dd>
                          </dl>
                        </div>--}}
                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>Full Name:</td>
                                    <td> {{$userDetails != false ? $userDetails['name'] : '-'}}</td>
                                </tr>
                                <tr>
                                    <td>Current Amount:</td>
                                    <td>$ {{$userDetails != false ? $userDetails['total_fund'] : '0'}}</td>
                                </tr>
                                <tr>
                                    <td>{{$userDetails != false ? ucwords($userDetails['skype_id']) : 'Contact type'}}:</td>
                                        <td> {{$userDetails != false ? $userDetails['alternative_contact_no'] : 'Not selected'}}</td>
                                </tr>

                                <tr>
                                <tr>
                                    <td>Gender:</td>
                                    <td> {{$userDetails != false ? $userDetails['gender'] : '-'}}</td>
                                </tr>
                                <tr>
                                    <td>Contact No:</td>
                                    <td> {{$userDetails != false ? $userDetails['contact_no'] : '-'}}</td>
                                </tr>
                                {{--<tr>
                                    <td>Contact No 2:</td>
                                    <td>
                                        {{$userDetails != false ? $userDetails['alternative_contact_no'] : '-'}}
                                    </td>
                                </tr>--}}
                                <tr>
                                    <td>Email:</td>
                                    <td>
                                        <a href="{{$userDetails != false ? $userDetails['email'] : '-'}}">
                                            {{$userDetails != false ? $userDetails['email'] : '-'}}
                                        </a>
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                            {{--<a href="#" class="btn btn-primary">My Sales Performance</a>--}}
                            {{--<a href="#" class="btn btn-primary">Team Sales Performance</a>--}}
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="{{url('/dashboard/contact-us')}}" data-original-title="Broadcast Message" data-toggle="tooltip" type="button"
                       class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                    <span class="pull-right">
                            <a href="{{url('/dashboard/user/profile/edit')}}" data-original-title="Edit this user"
                               data-toggle="tooltip" type="button"
                               class="btn btn-sm btn-success"><i class="glyphicon glyphicon-edit"></i> Update Profile</a>

                            <a  href="{{url('/dashboard/change/password')}}" data-original-title="Remove this user" data-toggle="tooltip" type="button"
                               class="btn btn-sm btn-success"><i class="glyphicon glyphicon-edit"></i> Change Password</a>
                        </span>
                </div>

            </div>
        </div>
    </div>
    </div>
@endsection