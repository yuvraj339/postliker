<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

@include('partial.adminHeader')

<body>

@include('partial.adminNavbar')

<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
        <li class="active">{{ Session()->has('selectedPage') ? Session()->get('selectedPage') : ''}}</li>
    </ul>
    <div class="row profile">

        <div class="col-md-3">
            <div class="profile-sidebar">

                @include('partial.sidebar')

            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">

                @yield('dashboard')

            </div>
        </div>
    </div>
</div>
{{--<center>
    <strong>Powered by <a href="http://j.mp/metronictheme" target="_blank">KeenThemes</a></strong>
</center>--}}
<br>
<br>
</body>
<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/adminSide.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-1.12.4.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.dataTables.min.js')}}" type="text/javascript"></script>

@yield('javascript')
@if($alert = Session::get('am-alert'))
    <script>
        jQuery.noConflict();

        function alerthide() {
            jQuery('.alert').hide(100);
        }
        jQuery(".alert-{{ $alert['type'] }}").fadeOut(5000);
    </script>
@endif
</html>
