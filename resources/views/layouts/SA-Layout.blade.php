<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>

    @include('superAdmin.partial.SA-Header')

</head>
<body>

@include('superAdmin.partial.SA-SideMenu')
<style>
    .hvr-bounce-to-right.active, i.nav_icon {
        background-color: rgb(217,84,89);
        color: white;
        /*-webkit-box-shadow: 3px 3px 5px buttonface;*/
        /*-moz-box-shadow: 3px 3px 5px buttonface;*/
        /*box-shadow: 3px 3px 5px buttonface;*/
    }
    .hvr-bounce-to-right.active:hover, i.nav_icon {
        color: #7F8C8D;
    }
    i.nav_icon {
        color: white;
    }
</style>
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
            @yield('content')

            @include('superAdmin.partial.SA-Footer')
        </div>
    </div>
</div>
<!---->
<!--scrolling js-->
{{--<script src="{{url('/')}}/superAdmin/js/scripts.js"></script>--}}
<script src="{{url('/')}}/superAdmin/js/jquery.nicescroll.js"></script>
<!--//scrolling js-->
<script src="{{url('/')}}/superAdmin/js/bootstrap.min.js"></script>

<script src="{{asset('js/jquery.dataTables.min.js')}}" type="text/javascript"></script>

</body>
@yield('javascript')


{{--@if($alert = Session::get('am-alert'))--}}
    {{--<script>--}}
        {{--jQuery.noConflict();--}}

        {{--function alerthide() {--}}
            {{--jQuery('.alert').hide(100);--}}
        {{--}--}}
        {{--jQuery(".alert-{{ $alert['type'] }}").fadeOut(5000);--}}
    {{--</script>--}}
{{--@endif--}}
<script>
    $(document).ready(function () {
        var mySelect = $('#first-disabled2');

        $('#special').on('click', function () {
            mySelect.find('option:selected').prop('disabled', true);
            mySelect.selectpicker('refresh');
        });

        $('#special2').on('click', function () {
            mySelect.find('option:disabled').prop('disabled', false);
            mySelect.selectpicker('refresh');
        });

        $('#basic2').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });
    });
</script>
</html>
