<!DOCTYPE html>
<html lang="en">
<head>

     <!-- For-Mobile-Apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Arbitrary a Responsive Web Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible Web Template, Smartphone Compatible Web Template, Free Webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //For-Mobile-Apps -->
    <!-- Custom-Stylesheet-Links -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css" media="all"/>
    <!-- //Custom-Stylesheet-Links -->
    <!-- Web-Fonts -->
    <!-- //Web-Fonts -->
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <!--gallery-->
    <!--//end-animate-->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    {{--<link href="/css/app.css" rel="stylesheet">--}}

    <!-- Scripts -->
    <script>
        window.Laravel = '<?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>';
    </script>
</head>
<body>
    <div id="app">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Liker') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                {{--<ul class="nav navbar-nav">
                    &nbsp;
                </ul>--}}

                <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        {{--<li class="sidebar-brand">
                            <a href="#top" onclick=$("#menu-close").click();>Liker</a>
                        </li>--}}
                        <li>
                            <a href="{{url('/#top')}}" onclick=$("#menu-close").click();>Home</a>
                        </li>
                        <li>
                            <a href="{{url('/#about')}}" onclick=$("#menu-close").click();>About</a>
                        </li>
                        <li>
                            <a href="{{url('/#services')}}" onclick=$("#menu-close").click();>Services</a>
                        </li>
                        <li>
                            <a href="{{url('/#contact')}}" onclick=$("#menu-close").click();>Contact</a>
                        </li>
                        @if (!Auth::check())
                            <li>
                                <a href="{{ url('/login') }}" onclick=$("#menu-close").click();>Login</a>
                            </li>
                            <li>
                                <a href="{{ url('/register') }}" onclick=$("#menu-close").click();>Register</a>
                            </li>
                        @else
                            <li class="dropdown">
                                <a href="{{ url('/dashboard') }}" onclick=$("#menu-close").click(); class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ucfirst(Auth::user()->name)}} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('dashboard/user/profile') }}">Profile</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/dashboard') }}">Dasboard</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
         @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
