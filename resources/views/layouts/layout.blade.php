<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>{{ config('app.name', 'Liker') }}</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <link href="{{url('/')}}/homepage/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="{{url('/')}}/homepage/css/font-awesome.css" rel="stylesheet">
    <link href="{{url('/')}}/homepage/css/simpleLightbox.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('/')}}/homepage/css/text-style.css"> <!--Banner-text-style -->
    <link href="{{url('/')}}/homepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!--fonts-->
    <link href="//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!--//fonts-->
</head>
<body>
<!--banner start here-->
<div class="banner" id="home">
    <div class="header">
        <div class="header-main">
            <div class="container">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1><a  href="{{url('/')}}">{{ config('app.name', 'Liker') }}<span>Get popularity best auto liker</span></a></h1>
                    </div>
                    <!-- navbar-header -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="hvr-outline-in"><a href="{{ url('/') }}">Home</a></li>
                            {{--<li><a class="hvr-outline-in" href="#about">About</a></li>--}}
                            <li><a class="hvr-outline-in" href="#services">Service</a></li>
                            {{--<li><a class="hvr-outline-in" href="#contact">Contact</a></li>--}}
                            <li><a href="#experts" class="hvr-outline-in">Experts</a></li>
                            @if (!Auth::check())
                            <li class="dropdown menu__item">
                                <a href="#" class="dropdown-toggle menu__link hvr-outline-in"  data-toggle="dropdown" data-hover="Pages" role="button" aria-haspopup="true" aria-expanded="false">More<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    {{--<li><a class="scroll" href="#services">Service</a></li>--}}
                                    <li><a class="scroll" href="#contact">Contact</a></li>
                                    {{--<li><a href="#experts" class="scroll">Experts</a></li>--}}
                                    <li><a href="#about" class="scroll">About</a></li>
                                    {{--<li><a href="#experts" class="scroll">Client</a></li>--}}

                                        <li><a href="{{ url('/register') }}" class="">Register</a></li>
                                        <li><a href="{{ url('/login') }}" class="">Login</a></li>


                                </ul>
                            </li>
                                @else
                                <li class="dropdown menu__item">
                                    <a href="#" class="dropdown-toggle menu__link hvr-outline-in"  data-toggle="dropdown" data-hover="Pages" role="button" aria-haspopup="true" aria-expanded="false">{{ucfirst(Auth::user()->name)}}<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        {{--<li><a class="scroll" href="#services">Service</a></li>--}}
                                        <li><a class="scroll" href="#contact">Contact</a></li>
                                        {{--<li><a href="#experts" class="scroll">Experts</a></li>--}}
                                        <li><a href="#about" class="scroll">About</a></li>
                                        {{--<li><a href="#experts" class="scroll">Client</a></li>--}}

                                        <li>
                                            <a href="{{ url('dashboard/user/profile') }}">Profile</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/dashboard') }}">Dasboard</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>


                                    </ul>
                                </li>

                            @endif


                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </nav>
                <div class="clearfix"> </div>
            </div>
        </div>
            @yield('content')
        <!--footer-->
        <div class="copy">
            <div class="container">
                <p>© 2017 Get Popularity . All Rights Reserved | Design by <a href="http://www.aitworld.co.in/">AIT Service Corportion</a> </p>
            </div>
        </div>
        <!--/footer -->
        <!-- js -->
        <script type="text/javascript" src="{{url('/')}}/homepage/js/jquery-2.1.4.min.js"></script>
        <script src="{{url('/')}}/homepage/js/modernizr.js"></script>	<!--banner text -->
        <script src="{{url('/')}}/homepage/js/main.js"></script><!--banner text -->
        <!-- stats -->
        <script src="{{url('/')}}/homepage/js/jquery.waypoints.min.js"></script>
        <script src="{{url('/')}}/homepage/js/jquery.countup.js"></script>
        <script>
            $('.counter').countUp();
        </script>
        <!-- //stats -->
        <!-- Lightbox -->
        <script src="{{url('/')}}/homepage/js/simpleLightbox.js"></script>
        <script>
            $('.w3layouts_gallery_grid a').simpleLightbox();
        </script>
        <!-- //Lightbox -->
        <script src="{{url('/')}}/homepage/js/SmoothScroll.min.js"></script>
        <!-- smooth scrolling -->
        <script type="text/javascript">
            $(document).ready(function() {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear'
                 };
                 */
                $().UItoTop({ easingType: 'easeOutQuart' });
            });
        </script>
        <a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
        <!-- //smooth scrolling -->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="{{url('/')}}/homepage/js/move-top.js"></script>
        <script type="text/javascript" src="{{url('/')}}/homepage/js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="{{url('/')}}/homepage/js/bootstrap-3.1.1.min.js"></script>
</body>
</html>