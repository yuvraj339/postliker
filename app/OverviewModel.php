<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OverviewModel extends Model
{
    protected $fillable =[
        'overview_heading',
        'overview_description',
    ];
}
