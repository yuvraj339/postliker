<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceRule extends Model
{
    protected $fillable = [
        'user_group',
        'user_id',
        'main_category_id',
        'sub_category_id',
        'price',
        'price_on_unit',
        'status',
    ];

    /**
     * @param $userDetails
     * @param $subCategoryId
     */
    public static function checkPriceRuleForThisSubCategory($userId, $subCategoryId)
    {
       return static::where('user_id',$userId)->where('sub_category_id',$subCategoryId)->value('price');
    }
}
