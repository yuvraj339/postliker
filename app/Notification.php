<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable =
        [
            'user_id',
            'user_type',
            'notification_details',
            'global_notification',
            'user_notification_type',
            'order_id',
            'status',
            'order_reference_number'
        ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //Below single function is using for save notifications in table
//    public static function updateNotificationTable($userId,$user_notification,$user_notification_type, $userType)
//    {
//        //for update user notification counter
//        (new User())->updateNotificationCount($userId);
//
//        return static::create([
//            'user_id' => $userId,
//            'user_type' => $userType,
//            'user_notification' => $user_notification,
//            'user_notification_type' => $user_notification_type,]);
//    }
//    public static function updateNotificationTableForAPI($userId,$user_notification,$user_notification_type, $userType,$testBookingId,$bookingRefrenceNumber)
//    {
//
//        //for update user notification counter
//        (new User())->updateNotificationCount($userId);
//
//        return static::create([
//            'user_id' => $userId,
//            'user_type' => $userType,
//            'user_notification' => $user_notification,
//            'user_notification_type' => $user_notification_type,
//            'booking_reference_number' => $bookingRefrenceNumber,
//            'booking_id' => $testBookingId]);
//    }

//    public static function showNotificationOnNavbar($userId)
//    {
//        return static::where('user_id','=',$userId)->orderBy('created_at','desc')->get();
//    }
}
