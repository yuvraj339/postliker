<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'service_id',
        'brand',
        'service_name',
        'price_per_fixed_amount',
        'min_order',
        'max_order',
        'details',
        'status',
        'user_type',
        'main_category_id',
        'sub_category_id',
    ];

    public function service()
    {
        return $this->hasMany('App\SubCategory');
    }
}
