<?php

namespace App;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class AccountModel extends Model
{
    protected $fillable =[
        'user_id',
        'fund_added_by',
        'fund_credit_debit_value',
        'fund_credit_status',
    ];

    /**
     * @param Request $request
     */
    public static function createFundDetails(Request $request)
    {
        return static::create(
            [
                'user_id' => $request->user_id,
                'fund_added_by' => Auth::user()->id,
                'fund_credit_debit_value' => $request->fund,
                'fund_credit_status' => '1',
            ]
        );
    }
    /**
     * @param Request $request
     */
    public static function createFundDetailsAfterOrder(Request $request,$actualAmountForQuantity)
    {
        if(Auth::user()->user_type == 'superadmin')
        {
            $userId = $request->user_id;
        }
        else
        {
            $userId = Auth::user()->id;
        }
        return static::create(
            [
                'user_id' => $userId,
                'fund_added_by' => Auth::user()->id,
                'fund_credit_debit_value' => $actualAmountForQuantity,
                'fund_credit_status' => '0',
            ]
        );
    }
}
