<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderHistory extends Model
{
    protected $fillable =[
      'status',//pending // completed / processing //
      'sub_category_id',
      'amount',
      'order_id',
      'order_on_url',
      'scrf',
      'user_id',
      'order_price',
      'order_date',
      'start_count',
    ];

    public static function updateOrderTableByApi($orderDetails,$id)
    {
        static::where('id',$id)->update([
            'status'=>$orderDetails['orderStatus'],
            'order_price'=>$orderDetails['orderPrice'],
            'order_date'=>$orderDetails['orderDate'],
            'start_count'=>$orderDetails['startCount'],
        ]);
    }

    /**
     * Rule for admin
     * @param Request $request
     * @return mixed
     */
    public static function ValidationRuleForAddOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'main_category_id' => 'required',
            'sub_category_id' => 'required',
            'order_on_url' => 'required',
            'amount' => 'required',
        ]);
        return $validator;
    }

    /**
     * Rule for user / retailer
     * @param Request $request
     * @return mixed
     */
    public static function ValidationRuleForAddOrderForUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'main_category_id' => 'required',
            'sub_category_id' => 'required',
            'order_on_url' => 'required',
            'amount' => 'required',
        ]);
        return $validator;
    }
}
