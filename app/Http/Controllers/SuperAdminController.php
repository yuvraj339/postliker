<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use App\Service;
use App\PriceRule;
use App\SubCategory;
use App\MainCategory;
use App\Notification;
use App\AccountModel;
use App\OrderHistory;
use App\ContactDetail;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use App\Acme\HelperClass;

class SuperAdminController extends HelperClass
{
    /**
     * SuperAdminController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

//        if(!Auth::guest())
//        {
//            if(Auth::user()->user_type != 'superadmin' || Auth::user()->id != 2)
//            {
//                return redirect()->back();
//            }
//        }
//        else
//        {
//            return redirect('/');
//        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('superAdmin.superAdminDashboard');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addUser()
    {
        return view('superAdmin.user.SA-AddUser');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|max:250|unique:users,email'
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $getDetails = array_except($request->all(), ['_token', 'password', 'profile_img']);

        if (Input::hasFile('profile_img')) {

            $destinationPath = 'uploads/profileImg/'; // upload path

            $extension = Input::file('profile_img')->getClientOriginalExtension(); // getting image extension

            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image

            Input::file('profile_img')->move($destinationPath, $fileName); // uploading file to given path
            $getDetails = array_merge($getDetails, ['profile_img' => $fileName]);
        }
        $getDetails = array_merge($getDetails, ['password' => bcrypt($request->password)]);

        $userDetails = User::create($getDetails);

        $alert = [
            'type' => $userDetails->id ? 'success' : 'warning',
            'data' => $userDetails->id ? ['Success : New user added successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateUser(Request $request, $userId)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|max:250|unique:users,email,' . $userId
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $getDetails = array_except($request->all(), ['_token', 'password', 'profile_img']);

        if (Input::hasFile('profile_img')) {

            $destinationPath = 'uploads/profileImg/'; // upload path

            $extension = Input::file('profile_img')->getClientOriginalExtension(); // getting image extension

            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image

            Input::file('profile_img')->move($destinationPath, $fileName); // uploading file to given path
            $getDetails = array_merge($getDetails, ['profile_img' => $fileName]);
        }

        if ($request->password != '') {
            $getDetails = array_merge($getDetails, ['password' => bcrypt($request->password)]);
        }

        $userDetails = User::find($userId);
        $userDetails->update($getDetails);
        $alert = [
            'type' => $userId ? 'success' : 'warning',
            'data' => $userId ? ['User profile has been updated successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewUser()
    {
        $userDetails = User::where('user_type', '!=', 'superadmin')->orderBy('id','desc')->get();

        return view('superAdmin.user.SA-ViewUser', compact('userDetails'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editUser($userId)
    {
        $userDetails = User::find($userId);

        return view('superAdmin.user.SA-EditUser', compact('userDetails'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deleteUser($userId)
    {
        $userDetails = User::find($userId);
        if ($userDetails != null) {
            $userDetails->delete();
        }
        $alert = [
            'type' => $userId ? 'success' : 'warning',
            'data' => $userId ? ['User has been deleted successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function groupUser()
    {
        return view('superAdmin.user.SA-GroupUser');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addFund()
    {
        $userList = User::where('user_type', '!=', 'superadmin')->pluck('email', 'id');
        return view('superAdmin.fund.SA-AddFund', compact('userList'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewFund()
    {
        $fundDetails = AccountModel::orderBy('id','desc')->get();
        $userDetails = User::pluck('name', 'id');
        return view('superAdmin.fund.SA-ViewFund', compact('fundDetails', 'userDetails'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeFund(Request $request)
    {
        //check admin have enough fund
        if($request->fund <= Auth::user()->total_fund)
        {
            $userDetails = User::find($request->user_id);
            $totalFund = (float)$request->fund + (float)$userDetails->total_fund;

            $userDetails->update(['total_fund' => (string)$totalFund]);

            User::where('id',Auth::user()->id)->update(['total_fund' => (float)Auth::user()->total_fund - (float)$request->fund]);

            AccountModel::createFundDetails($request);

            $alert = [
                'type' => 'success',
                'data' => ['Fund successfully transfer to user.'],
            ];
        }
        else
        {
            $alert = [
                'type' => 'danger',
                'data' => ['Sorry you do not have enough found'],
            ];
        }

        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addOrder()
    {
        $userList = User::where('user_type', '!=', 'superadmin')->pluck('email', 'id');
        $serviceList = Service::pluck('service_name', 'service_id');
        $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id');

        return view('superAdmin.orderHistory.SA-OrderAdd', compact('userList', 'serviceList', 'mainCategories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeOrder(Request $request)
    {
        //set validation
        $validator = OrderHistory::ValidationRuleForAddOrder($request);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator->errors());
        }
        $getOrderStatus = OrderHistory::where('order_on_url',$request->order_on_url)
            ->where('user_id',$request->user_id)->value('status');
         if ($getOrderStatus != 'Completed' && $getOrderStatus != null) {

            $alert = [
                'type' => 'warning',
                'data' => ['Order in progress'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
        $serviceDetails = Service::where('sub_category_id', $request->sub_category_id)->first();

    if($serviceDetails == null)
    {
        $alert = [
            'type' => 'warning',
            'data' => ['Please add service of selected category.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }
        //check order is between
        if ($request->amount >= $serviceDetails->min_order && $request->amount <= $serviceDetails->max_order) {
            $userDetails = User::find($request->user_id);

            //set order price and check amount is available or not
            $actualAmountForQuantity = $this->getPriceBySubCategoryId($request, $userDetails, $serviceDetails);

            if ($userDetails->total_fund >= $actualAmountForQuantity) {
                $getDetailsFromApiAfterOrder = $this->addOrderAPI($request->order_on_url, $request->sub_category_id, $request->amount);

                $jsonDecodedData = json_decode($getDetailsFromApiAfterOrder, true);

                if (isset($jsonDecodedData['error'])) {
                    if($jsonDecodedData['error'] ='incorrect service type')
                    {
                        $jsonDecodedData['orderID'] = 'Not-PL';
                    }
                    else
                    {
                        $alert = [
                            'type' => 'warning',
                            'data' => [$jsonDecodedData['error']],
                        ];
                        return redirect()->back()->withInput()->with('am-alert', $alert);
                    }
                }
                $RemainFund = (float)$userDetails->total_fund - (float)$actualAmountForQuantity;

                $userDetails->update(['total_fund' => (string)$RemainFund]);
//                User::where('user_type', 'superadmin')->update(['total_fund' => $jsonDecodedData['remaining_balance']]);

                $orderHistory = array_merge(
                    array_except($request->all(), '_token'),
                    ['status' => 'pending','order_id' => $jsonDecodedData['orderID']]
                );
//                print_r($jsonDecodedData);
                AccountModel::createFundDetailsAfterOrder($request,$actualAmountForQuantity);
                OrderHistory::create($orderHistory);
                $alert = [
                    'type' =>  'success' ,
                    'data' => ['Order placed successfully.'],
                ];
                    return redirect('super/admin/order/view')->withInput()->with('am-alert', $alert);
            }
            else
            {
                $alert = [
                    'type' => 'warning',
                    'data' => ['Sorry! User Have not enough balance.'],
                ];
            }

        } else {
            $alert = [
                'type' => 'warning',
                'data' => ["Please enter order amount between $serviceDetails->min_order to $serviceDetails->max_order"],
            ];
        }


        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function historyOrder()
    {
        $orderDetails = OrderHistory::orderBy('id','desc')->where('order_id','!=','Not-PL')->get();
        $userDetails = User::pluck('name', 'id');
        $serviceDetails = SubCategory::pluck('sub_service_name', 'service_id');
        return view('superAdmin.orderHistory.SA-OrderView', compact('orderDetails', 'userDetails', 'serviceDetails'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function NPLhistoryOrder()
    {
        $orderDetails = OrderHistory::orderBy('id','desc')->where('order_id','=','Not-PL')->get();
        $userDetails = User::pluck('name', 'id');
        $serviceDetails = SubCategory::pluck('sub_service_name', 'service_id');
        return view('superAdmin.orderHistory.SA-OrderViewNPL', compact('orderDetails', 'userDetails', 'serviceDetails'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editNPLOrder($orderId)
    {
        $orderDetails = OrderHistory::find($orderId);
        return view('superAdmin.orderHistory.SA-OrderEditNPL', compact('orderDetails'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateNPLOrder($orderId, Request $request)
    {
//        dd($request->all());
        OrderHistory::where('id',$orderId)->update(array_except($request->all(), '_token'));
        $alert = [
            'type' => 'success',
            'data' => ['Order updated successfully'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteOrder($orderId)
    {
        $orderDetails = OrderHistory::find($orderId);
        if ($orderDetails != null) {
            $orderDetails->delete();
        }
        $alert = [
            'type' => $orderId ? 'success' : 'warning',
            'data' => $orderId ? ['Order deleted successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addService()
    {
        $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id');
        return view('superAdmin.service.SA-ServiceAdd', compact('mainCategories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeService(Request $request)
    {
//        dd($request->all());
//        $validator = Validator::make($request->all(), [
//            'sub_category_id' => 'required|max:500|unique:services,service_id'
//        ]);
        /*$validator = Validator::make($request->all(), [
            'mix_order' => 'required|numeric',
            'max_order' => 'numeric'
        ]);
        if ($validator->fails()) {

            $alert = [
                'type' => 'danger',
                'data' => ['Min order / max order must be numeric.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }*/
        if ($request->sub_category_id == null) {
            $alert = [
                'type' => 'danger',
                'data' => ['Please select at least one sub category.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }

        $details = $request->details;
        if($request->details == '')
        {
            $details = '-';
        }
        Service::create(array_merge($request->all(),['details'=>$details]));

        $alert = [
            'type' => 'success',
            'data' => ['Service added successfully.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewService()
    {
        $subCategory = SubCategory::where('status', 1)->pluck('sub_service_name', 'service_id')->toArray();
        $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id')->toArray();
        $serviceDetails = Service::orderBy('id','desc')->get();
//        dd($serviceDetails,$mainCategories,$subCategory);
        return view('superAdmin.service.SA-ServiceView', compact('serviceDetails', 'subCategory', 'mainCategories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editService($serviceId)
    {
        $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id');
        $serviceDetails = Service::find($serviceId);
        /*$alert = [
            'type' => $serviceId ? 'warning' : 'warning',
            'data' => $serviceId ? ['Work in progress.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);*/
        return view('superAdmin.service.SA-ServiceEdit', compact('serviceDetails', 'mainCategories'));
    }

    /**
     * @param $serviceId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateService(Request $request, $serviceId)
    {

        if ($request->sub_category_id == null) {
            $alert = [
                'type' => 'danger',
                'data' => ['Please select at least one sub category.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
//        dd($serviceId,$request->all());
//        $mainCategories = MainCategory::where('status',1)->pluck('service_name','id');
        $serviceDetails = Service::where('id', $serviceId)->update(array_except($request->all(), ['_token']));
        $alert = [
            'type' => $serviceId ? 'success' : 'warning',
            'data' => $serviceId ? ['Service updated successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
//        return view('superAdmin.service.SA-ServiceEdit', compact('serviceDetails','mainCategories'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteService($serviceId)
    {
        $orderDetails = Service::find($serviceId);

        if ($orderDetails != null) {
            $orderDetails->delete();
        }

        $alert = [
            'type' => $orderDetails ? 'success' : 'warning',
            'data' => $orderDetails ? ['Service deleted successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addNotification()
    {
        $userList = User::where('user_type', '!=', 'superadmin')->pluck('email', 'id');

        return view('superAdmin.notification.SA-NotificationAdd', compact('userList'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeNotification(Request $request)
    {
        Notification::create(array_merge($request->all(), ['user_id' => Auth::user()->id]));

        $alert = [
            'type' => $request->service_id ? 'success' : 'warning',
            'data' => $request->service_id ? ['Notification posted successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewNotification()
    {
        $userDetails = User::pluck('name', 'id');
        $notificationDetails = Notification::orderBy('id','desc')->get();
        return view('superAdmin.notification.SA-NotificationView', compact('notificationDetails', 'userDetails'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editNotification($serviceId)
    {
        $alert = [
            'type' => $serviceId ? 'warning' : 'warning',
            'data' => $serviceId ? ['Work in progress.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteNotification($serviceId)
    {
        $orderDetails = Service::find($serviceId);

        if ($orderDetails != null) {
            $orderDetails->delete();
        }


        $alert = [
            'type' => $orderDetails ? 'success' : 'warning',
            'data' => $orderDetails ? ['Service deleted successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addPriceRule()
    {
        $userList = User::where('user_type', '!=', 'superadmin')->pluck('email', 'id');
        $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id');
        return view('superAdmin.priceRule.SA-PriceRuleAdd', compact('mainCategories', 'userList'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storePriceRule(Request $request)
    {
        if ($request->sub_category_id == null) {
            $alert = [
                'type' => 'danger',
                'data' => ['Please select at least one sub category.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
        if ($request->price == null) {
            $alert = [
                'type' => 'danger',
                'data' => ['Please set price.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
        PriceRule::create($request->all());

        $alert = [
            'type' => $request->user_id ? 'success' : 'warning',
            'data' => $request->user_id ? ['Service added successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewPriceRule()
    {
        $userList = User::where('user_type', '!=', 'superadmin')->pluck('email', 'id');
        $subCategories = SubCategory::where('status', 1)->pluck('sub_service_name', 'id');
        $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id');
        $priceDetails = PriceRule::orderBy('id','desc')->get();
        return view('superAdmin.priceRule.SA-PriceRuleView', compact('priceDetails', 'mainCategories', 'userList', 'subCategories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editPriceRule($priceId)
    {
        $userList = User::where('user_type', '!=', 'superadmin')->pluck('email', 'id');
        $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id');
        $priceDetails = PriceRule::find($priceId);
        return view('superAdmin.priceRule.SA-PriceRuleEdit', compact('priceDetails', 'mainCategories', 'userList'));
    }

    /**
     * @param Request $request
     * @param $priceId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updatePriceRule(Request $request, $priceId)
    {
        if ($request->sub_category_id == null) {
            $alert = [
                'type' => 'danger',
                'data' => ['Please select at least one sub category.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
        if ($request->price == null) {
            $alert = [
                'type' => 'danger',
                'data' => ['Please set price.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
        $status = PriceRule::where('id', $priceId)->update(array_except($request->all(), '_token'));
        $alert = [
            'type' => $status != '' ? 'success' : 'warning',
            'data' => $status != '' ? ['Price rule updated successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletePriceRule($priceId)
    {
        $priceDetails = PriceRule::find($priceId);

        if ($priceDetails != null) {
            $priceDetails->delete();
        }

        $alert = [
            'type' => $priceDetails ? 'success' : 'warning',
            'data' => $priceDetails ? ['Price rule deleted successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inbox()
    {
        $contactDetails = ContactDetail::orderBy('id','desc')->get();
        return view('superAdmin.contact.SA-ContactView', compact('contactDetails'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categoryAdd(Request $request)
    {
        if ($request->all() == []) {
            $mainCategory = MainCategory::orderBy('id','desc')->get();
            return view('superAdmin.service.SA-ServiceAddMainCategoric', compact('mainCategory'));
        }

        $validator = Validator::make($request->all(), [
            'service_name' => 'required|max:500|unique:main_categories,service_name'
        ]);
        if ($validator->fails()) {

            $alert = [
                'type' => 'warning',
                'data' => $validator->errors()->toArray()['service_name'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        } else {
            $addMainService = MainCategory::create($request->all());
            $alert = [
                'type' => count($addMainService) > 0 ? 'success' : 'warning',
                'data' => count($addMainService) > 0 ? ['Main category of service added successfully.'] : ['Sorry! Something went wrong please try again.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
    }

    /**
     * @param $serviceId
     * @param $action
     * @return \Illuminate\Http\RedirectResponse
     */
    public function categoryEdit($serviceId, $action)
    {
        $getMainService = MainCategory::find($serviceId);
        if ($getMainService != null) {
            if ($action == 'status') {
                MainCategory::where('id', $serviceId)->update([
                    'status' => $getMainService->status == 1 ? 0 : 1
                ]);
                $status = $getMainService->status == 1 ? 'Disable' : 'Enable';
                $alert = [
                    'type' => 'success',
                    'data' => ["Your main category $status successfully"],
                ];
            } else {
                $getMainService->delete();
                $alert = [
                    'type' => 'success',
                    'data' => ['Your main category deleted successfully'],
                ];
            }
        } else {
            $alert = [
                'type' => $getMainService != null ? 'success' : 'warning',
                'data' => $getMainService != null ? ['Main category of service added successfully.'] : ['Sorry! Something went wrong please try again.'],
            ];
        }

        return redirect()->back()->withInput()->with('am-alert', $alert);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subCategoryAdd(Request $request)
    {
        if ($request->all() == []) {
            $subCategory = SubCategory::orderBy('id','desc')->get();
            $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id');
            return view('superAdmin.service.SA-ServiceAddSubCategoric', compact('subCategory', 'mainCategories'));
        }
//        $validator = Validator::make($request->all(), [
//            'service_id' => 'required|max:500|unique:services,service_id'
//        ]);
//        if ($validator->fails()) {
//            $alert = [
//                'type' =>  'warning',
//                'data' =>   $validator->errors()->toArray()['service_id'],
//            ];
//            return redirect()->back()->withInput()->with('am-alert', $alert);
//        }
        $validator = Validator::make($request->all(), [
            'sub_service_name' => 'required|max:500|unique:sub_categories,sub_service_name',
            'service_id' => 'required|max:250|unique:sub_categories,service_id'

        ]);
        if ($validator->fails()) {
            $alert = [
                'type' => 'warning',
                'data' => $validator->errors()->toArray()['sub_service_name'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        } else {
            $addSubService = SubCategory::create($request->all());
            $alert = [
                'type' => count($addSubService) > 0 ? 'success' : 'warning',
                'data' => count($addSubService) > 0 ? ['Sub category of service added successfully.'] : ['Sorry! Something went wrong please try again.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
    }

    /**
     * @param $serviceId
     * @param $action
     * @return \Illuminate\Http\RedirectResponse
     */
    public function subCategoryEdit($serviceId, $action)
    {
        $getMainService = SubCategory::find($serviceId);
        if ($getMainService != null) {
            if ($action == 'status') {
                SubCategory::where('id', $serviceId)->update([
                    'status' => $getMainService->status == 1 ? 0 : 1
                ]);
                $status = $getMainService->status == 1 ? 'Disable' : 'Enable';
                $alert = [
                    'type' => 'success',
                    'data' => ["Your sub category $status successfully"],
                ];
            } else {
                $getMainService->delete();
                $alert = [
                    'type' => 'success',
                    'data' => ['Your sub category deleted successfully'],
                ];
            }
        } else {
            $alert = [
                'type' => $getMainService != null ? 'success' : 'warning',
                'data' => $getMainService != null ? ['Main category of service added successfully.'] : ['Sorry! Something went wrong please try again.'],
            ];
        }

        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @param $mainCategoryId
     */
    public function loadSubCategories($mainCategoryId)
    {
//        $service = Service::where('main_category_id',$mainCategoryId)->get(['service_id','main_category_id'])->toArray();
        $subCategories = SubCategory::where('main_category_id', $mainCategoryId)->get(['sub_service_name', 'service_id'])->toArray();
        //        $result = [];
//        $i = 0;
//        foreach($subCategories as $subCat):
//            $result[] = [
//                'id'=>array_key_exists($service[$i]['main_category_id'],array_flip($subCat)) ? $service[$i]['service_id']:'-',
//                'sub_service_name'=>$subCat['sub_service_name']
//            ];
//        ++$i;
//        endforeach;

        return response($subCategories);
    }

    /**
     * @param $orderId
     * @return bool|mixed
     */
    public function getOrderStatus($id, $orderId)
    {
        $getDetailsFromApi = $this->orderStatusAPI($orderId);
        $jsonDecodedData = json_decode($getDetailsFromApi, true);
        OrderHistory::updateOrderTableByApi($jsonDecodedData, $id);
        return json_encode(['orderStatus' => $jsonDecodedData['orderStatus']]);
    }

}
