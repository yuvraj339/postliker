<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use App\Service;
use App\PriceRule;
use App\SubCategory;
use App\MainCategory;
use App\OrderHistory;
use App\AccountModel;
use App\Notification;
use App\ContactDetail;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_type == 'superadmin')
        {
            $view = 'superAdmin.superAdminDashboard';
            return view('superAdmin.superAdminDashboard');

        }
        else
        {
            if(Auth::user()->contact_no == '')
            {
                Session()->put('selectedPage', 'User profile');

                $userDetails = current(User::where('id', Auth::user()->id)->get()->toArray());

                return view('user.userProfile', compact('userDetails'));
            }
            else{
                Session()->put('selectedPage', 'Home');

                $orderDetails = OrderHistory::where('user_id',Auth::user()->id)->take(2)->orderBy('id','desc')->get();
                $userAccountDetails = AccountModel::where('user_id', Auth::user()->id)->orderBy('id','desc')->take(2)->get();
                $notificationDetails = Notification::Where('user_type',Auth::user()->user_type)->orderBy('id','desc')
                    ->orWhere('global_notification',1)
                    ->take(2)->get();
                return view('user.dashboard',compact('orderDetails','notificationDetails','userAccountDetails'));

            }

        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userProfile()
    {
        Session()->put('selectedPage', 'User profile');

        $userDetails = current(User::where('id', Auth::user()->id)->get()->toArray());

        return view('user.userProfile', compact('userDetails'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editUserProfile()
    {
        Session()->put('selectedPage', 'Edit Profile');

        $userDetails = User::find(Auth::user()->id);

        return view('user.editUserProfile', compact('userDetails'));
    }

    /**
     * @param $userId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updateUserProfile($userId, Request $request)
    {
        $validator = Validator::make($request->all(), [
//            'contact_no' => 'digits:10',
//            'alternative_contact_no' => 'digits:10',
//            'email'=> Rule::unique('users')->ignore($userId, 'id')
            'email' => 'required|max:250|unique:users,email,' . $userId
        ]);
        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $getDetailsForUpdate = array_except($request->all(), 'profile_img_upload');
        if (Input::hasFile('profile_img_upload')) {

            $destinationPath = 'uploads/profileImg/'; // upload path

            $extension = Input::file('profile_img_upload')->getClientOriginalExtension(); // getting image extension

            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image

            Input::file('profile_img_upload')->move($destinationPath, $fileName); // uploading file to given path
            $getDetailsForUpdate = array_merge($getDetailsForUpdate, ['profile_img' => $fileName]);
        }
        $userDetails = User::find($userId);
        $userDetails->update($getDetailsForUpdate);
        $alert = [
            'type' => $userId ? 'success' : 'warning',
            'data' => $userId ? ['Your profile has been updated successfully.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    public function changePassword()
    {
        Session()->put('selectedPage', 'Change Password');

        return view('user.changePassword');
    }

    /**
     * @param $userId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updatePassword($userId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $user = User::find(Auth::user()->id);

        if (Hash::check($request->old_password, $user->password) == false) {
            $alert = [
                'type' => 'danger',
                'data' => ['Sorry! old password does not match.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        } else {
            User::where('id', $userId)->update(['password' => bcrypt($request->password)]);
            $alert = [
                'type' => 'success',
                'data' => ['Your password has been changed successfully.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function overview()
    {
        Session()->put('selectedPage', 'Overview');

        $orderDetails = OrderHistory::where('user_id',Auth::user()->id)->take(2)->orderBy('id','desc')->get();
        $userAccountDetails = AccountModel::where('user_id', Auth::user()->id)->orderBy('id','desc')->take(2)->get();
        $notificationDetails = Notification::Where('user_type',Auth::user()->user_type)->orderBy('id','desc')
            ->orWhere('global_notification',1)
            ->take(2)->get();
        return view('user.overview',compact('orderDetails','notificationDetails','userAccountDetails'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contactUs()
    {
        Session()->put('selectedPage', 'Contact Us');

        return view('user.contactUs');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeContactUs(Request $request)
    {
        ContactDetail::create($request->all());

        $alert = [
            'type' => Auth::user()->id ? 'success' : 'warning',
            'data' => Auth::user()->id ? ['Your message has been sent successfully we will contact you soon.'] : ['Sorry! Something went wrong please try again.'],
        ];
        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function priceInfo()
    {
        Session()->put('selectedPage', 'Price Info');

        $serviceDetails = Service::where('user_type',Auth::user()->user_type)->where('status',1)->get();
        $subCategory = SubCategory::where('status', 1)->pluck('sub_service_name', 'service_id')->toArray();
        $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id')->toArray();
        $priceRule = PriceRule::where('user_id',Auth::user()->id)->pluck('price','sub_category_id')->toArray();
        return view('user.priceInfo',compact('priceRule','subCategory','mainCategories','serviceDetails'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userNotification()
    {
        Session()->put('selectedPage', 'Notification');

//        $userDetails = User::pluck('name', 'id');
        $notificationDetails = Notification::Where('user_type',Auth::user()->user_type)
            ->orWhere('global_notification',1)
            ->get();
//        dd($notificationDetails);
        return view('user.notification', compact('notificationDetails','userDetails'));
    }
}

