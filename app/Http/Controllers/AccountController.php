<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Service;
use App\SubCategory;
use App\AccountModel;
use App\OrderHistory;
use App\MainCategory;
use App\Acme\HelperClass;
use Illuminate\Http\Request;

class AccountController extends HelperClass
{
    /**
     * AccountController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Session()->put('selectedPage', 'Account');

        $userAccountDetails = AccountModel::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        $userDetails = User::pluck('name', 'id');

        return view('user.accountView', compact('userAccountDetails', 'userDetails'));
    }

    public function orderAdd()
    {
        Session()->put('selectedPage', 'Order Add');

        $mainCategories = MainCategory::where('status', 1)->pluck('service_name', 'id');
//        $serviceList = Service::pluck('service_name','service_id');
        return view('user.order.orderAdd', compact('mainCategories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orderStore(Request $request)
    {
        //set validation
        $validator = OrderHistory::ValidationRuleForAddOrderForUser($request);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator->errors());
        }
        $getOrderStatus = OrderHistory::where('order_on_url',$request->order_on_url)
            ->where('user_id',Auth::user()->id)->value('status');
        if ($getOrderStatus != 'Completed'  && $getOrderStatus != null) {

            $alert = [
                'type' => 'warning',
                'data' => ['Order in progress'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
        $serviceDetails = Service::where('sub_category_id', $request->sub_category_id)->first();
        if($serviceDetails == null)
        {
            $alert = [
                'type' => 'warning',
                'data' => ['Please add service of selected category.'],
            ];
            return redirect()->back()->withInput()->with('am-alert', $alert);
        }
        //check order is between
        if ($request->amount >= $serviceDetails->min_order && $request->amount <= $serviceDetails->max_order) {
            $userDetails = User::find(Auth::user()->id);

            //set order price and check amount is available or not
            $actualAmountForQuantity = $this->getPriceBySubCategoryId($request, $userDetails, $serviceDetails);

            if ($userDetails->total_fund >= $actualAmountForQuantity) {

                $getDetailsFromApiAfterOrder = $this->addOrderAPI($request->order_on_url, $request->sub_category_id, $request->amount);

                $jsonDecodedData = json_decode($getDetailsFromApiAfterOrder, true);

                if (isset($jsonDecodedData['error'])) {
                    if($jsonDecodedData['error'] ='incorrect service type')
                    {
                        $jsonDecodedData['orderID'] = 'Not-PL';
                    }
                    else
                    {
                        $alert = [
                            'type' => 'warning',
                            'data' => [$jsonDecodedData['error']],
                        ];
                        return redirect()->back()->withInput()->with('am-alert', $alert);
                    }
                }
//                if (isset($jsonDecodedData['error'])) {
//
//                }
                $RemainFund = (float)$userDetails->total_fund - (float)$actualAmountForQuantity;

                $userDetails->update(['total_fund' => (string)$RemainFund]);
//                User::where('user_type', 'superadmin')->update(['total_fund' => $jsonDecodedData['remaining_balance']]);

                $orderHistory = array_merge(
                    array_except($request->all(), '_token'),
                    [
                        'status' => 'pending',
                        'order_id' => $jsonDecodedData['orderID'],
                        'user_id' => Auth::user()->id
                    ]
                );
//                print_r($jsonDecodedData);
                AccountModel::createFundDetailsAfterOrder($request, $actualAmountForQuantity);
                OrderHistory::create($orderHistory);
                $alert = [
                    'type' => 'success',
                    'data' => ['Order placed successfully.'],
                ];
                return redirect('/dashboard/order/history')->withInput()->with('am-alert', $alert);

            } else {
                $alert = [
                    'type' => 'warning',
                    'data' => ['Sorry! User Have not enough balance.'],
                ];
            }

        } else {
            $alert = [
                'type' => 'warning',
                'data' => ["Please enter order amount between $serviceDetails->min_order to $serviceDetails->max_order"],
            ];
        }


        return redirect()->back()->withInput()->with('am-alert', $alert);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orderHistory()
    {
        Session()->put('selectedPage', 'Order History');

        $orderDetails = OrderHistory::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
//        $userDetails = User::pluck('name', 'id');
        $serviceDetails = SubCategory::pluck('sub_service_name', 'service_id');

        return view('user.order.orderHistory', compact('orderDetails', 'serviceDetails'));
    }

    /**
     * @param $amount
     * @param $subCateId
     * @return string
     */
    public function getActualAmount($subCateId, $amount, $userId = null)
    {
        if($userId == null)
        {
            $userId = Auth::user()->id;
        }

        $serviceDetails = Service::where('sub_category_id', $subCateId)->first();
        $actualPrice = $this->getPriceBySubCategoryIdForFrontUse($amount,$subCateId,$userId,$serviceDetails);
        return json_encode(['totalAmount' => $actualPrice]);
    }
}
