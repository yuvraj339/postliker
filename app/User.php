<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'api_key',
        'email',
        'password',
        'user_status',
        'user_type',
        'profile_img',
        'total_fund',
        'skype_id', //contact_type
        'gender',
        'address',
        'contact_no',
        'alternative_contact_no', //contact_type_info
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param $value : status
     * @param $byField : $request->email
     * @param $fieldName : email
     */
    public static function getValueByField($value, $byField, $fieldName)
    {
        return static::where($fieldName,$byField)->value($value);
    }
}
