<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = [
        'main_category_id',
        'status',
        'sub_service_name',
        'service_id',
    ];

    public function subCategory()
    {
        return $this->hasMany('App\Service');
    }
}
