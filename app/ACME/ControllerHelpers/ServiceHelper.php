<?php
/**
 * Created by PhpStorm.
 * User: yuvraj
 * Date: 8/4/17
 * Time: 4:58 PM
 */

namespace App\Acme\ControllerHelpers;


use App\Http\Controllers\Controller;

class ServiceHelper extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function addService()
    {
        return view('superAdmin.service.SA-ServiceAdd');
    }
}