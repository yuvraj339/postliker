<?php

namespace App\Acme;

use App\PriceRule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SuperAdminController;

class HelperClass extends Controller
{
    protected $apiURl = 'http://www.postlikes.com/api.php';
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $userId
     */
    public function convertUserIdToName($userId)
    {
        return User::where('id',$userId)->value('name');
    }

    /**
     * @param $orderUrl
     * @param $orderType
     * @param $orderQuantity
     * @param $apiURl
     * @return bool|mixed
     */
    public function addOrderAPI($orderUrl, $orderType, $orderQuantity)
    {
        header('Content-Type: application/json');

        $apiKey = Auth::user()->api_key;
        if(Auth::user()->api_key == null)
        {
            $apiKey = User::where('user_type','superadmin')->value('api_key');
        }

        $params = array(
            'apiKey' => $apiKey,
            'actionType' => 'add',
            'orderType' => $orderType,
            'orderUrl' => $orderUrl,
            'orderQuantity' => $orderQuantity
        );
        $result = self::ConnectToAPI($params, $this->apiURl);
        return $result;
    }

    /**
     * @param $orderID
     * @param $apiURl
     * @return bool|mixed
     */
    public function orderStatusAPI($orderID)
    {
        header('Content-Type: application/json');

        $apiKey = Auth::user()->api_key;
        if(Auth::user()->api_key == null)
        {
           $apiKey = User::where('user_type','superadmin')->value('api_key');
        }

        $params = array(
            'apiKey' => $apiKey,
            'actionType' => 'status',
            'orderID' => $orderID
        );
        $result = self::ConnectToAPI($params, $this->apiURl);
        return $result;
    }

    /**
     * @param $params
     * @param $apiURl
     * @return bool|mixed
     */
    protected function ConnectToAPI($params, $apiURl)
    {

        $post = array();
        if (is_array($params)) {
            foreach ($params as $key => $value) {
                $post[] = $key . '=' . urlencode($value);
            }
        }

        $c = curl_init();

        curl_setopt($c, CURLOPT_URL, $apiURl);
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, join('&', $post));
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($c, CURLOPT_TIMEOUT, 10);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; tr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13');
        $contents = curl_exec($c);
        curl_close($c);
        if ($contents) return $contents;
        else return FALSE;
    }

    /**
     * @param Request $request
     * @param $userDetails
     * @param $serviceDetails
     * @return mixed
     */
    public function getPriceBySubCategoryId(Request $request, $userDetails, $serviceDetails)
    {
        $checkPriceRuleForThisSubCategory = PriceRule::checkPriceRuleForThisSubCategory($userDetails->id, $request->sub_category_id);
        if ($checkPriceRuleForThisSubCategory == null) {
            $checkPriceRuleForThisSubCategory = $serviceDetails->price_per_fixed_amount;
        }
        $actualAmountForQuantity = ($checkPriceRuleForThisSubCategory / $serviceDetails->min_order) * $request->amount;

        return $actualAmountForQuantity;
    }

    /**
     * @param Request $request
     * @param $userDetails
     * @param $serviceDetails
     * @return mixed
     */
    public function getPriceBySubCategoryIdForFrontUse($amount,$subCateId, $userId, $serviceDetails)
    {
        $checkPriceRuleForThisSubCategory = PriceRule::checkPriceRuleForThisSubCategory($userId, $subCateId);
        if ($checkPriceRuleForThisSubCategory == null) {
            $checkPriceRuleForThisSubCategory = $serviceDetails->price_per_fixed_amount;
        }
        $actualAmountForQuantity = ($checkPriceRuleForThisSubCategory / $serviceDetails->min_order) * $amount;

        return $actualAmountForQuantity;
    }
}